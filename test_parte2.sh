#/bin/sh
DEFAULTITERATIONSNUM=10
if (( $# == 0 ))
then
	iterations=$DEFAULTITERATIONSNUM;
else
	iterations=$0;
fi
echo "$iterations iterations";

## triangolazioni4
#files=( 'marcy128' 'marcy128_20' 'marcy128_50' 'marcy_1k' 'marcy_5k' 'marcy_20k');
##triangolazioni3
#files=('griglia_100' 'griglia_1k' 'griglia_14k');
##triangolazioni2
#files=('dolomiti10k' 'dolomiti20k' 'dolomiti30k');
##triangolazioni1
#files=('esempio' 'altroesempio');
##tutte le triangolazioni
files=('esempio' 'altroesempio' 'dolomiti10k' 'dolomiti20k' 'dolomiti30k' 'griglia_100' 'griglia_1k' 'griglia_14k' 'marcy128' 'marcy128_20' 'marcy128_50' 'marcy_1k' 'marcy_5k' 'marcy_20k');
dts=( '-list' '-grid' '-qtree');
for file in "${files[@]}"
do
	echo "testing file $file"; 
	for dt in "${dts[@]}"
	do
		echo "	testing dt $dt";
		min=9999999999999;
		ntr=0;
		## get time usage
		for (( i=0 ; i<iterations ; i++ ))
		do
			echo "iteration $i";
			res=$(./parte2 $dt -profiling ./TestFiles/$file);
			time=${res#* };
			time=${time%% *};
			if (( time < min ))
			then
				min=$time;
				ntr=${res%% *};
			fi
		done;
		## get memory usage
		res=$(./parte2 $dt -memcheck ./TestFiles/$file);
		minsize=$(echo $res | awk '{print $2}');
		maxsize=$(echo $res | awk '{print $3}');
		avrgsize=$(echo $res | awk '{print $4}');
		echo "final result:"
		echo 'N.triangles    min_time    min_mem    max_mem   avrg_mem'
		printf ' %10d  %10d %10d %10d %10s\n' $ntr $min $minsize $maxsize $avrgsize;
		echo "$ntr $min $minsize $maxsize $avrgsize $file">>"./Results/results$dt"
	done;
done;
for dt in "${dts[@]}"
do
	sort -nk1 "./Results/results$dt">./Results/results$dt-sorted;
done;

#############################
## test diffent grid edges ##
echo "\n\ntesting different grid edges...\n"
edges=('10' '100' '1000' '5000' '7500' '10000');
for file in "${files[@]}"
do
	echo "testing file $file"; 
	for edge in "${edges[@]}"
	do
		echo "	testing edge= $edge";
		min=9999999999999;
		ntr=0;
		## get time usage
		for (( i=0 ; i<iterations ; i++ ))
		do
			echo "iteration $i";
			res=$(./parte2 -grid $edge -profiling ./TestFiles/$file);
			time=${res#* };
			time=${time%% *};
			if (( time < min ))
			then
				min=$time;
				ntr=${res%% *};
			fi
		done;
		## get memory usage
		res=$(./parte2 -grid $edge -memcheck ./TestFiles/$file);
		minsize=$(echo $res | awk '{print $2}');
		maxsize=$(echo $res | awk '{print $3}');
		avrgsize=$(echo $res | awk '{print $4}');
		echo "final result:"
		echo 'N.triangles    min_time    min_mem    max_mem   avrg_mem'
		printf ' %10d  %10d %10d %10d %10s\n' $ntr $min $minsize $maxsize $avrgsize;
		echo "$ntr $min $minsize $maxsize $avrgsize $file">>"./Results/results_grid_e$edge"
	done;
done;
for esge in "${edges[@]}"
do
	sort -nk1 "./Results/results_grid_e$edge">./Results/results_grid_e$edge-sorted;
done;
echo "\ntesting grid edges on dolomiti30k..."
for edge in "${edges[@]}"
do
	echo "	testing edge= $edge";
	min=9999999999999;
	ntr=0;
	## get time usage
	for (( i=0 ; i<iterations ; i++ ))
	do
		echo "iteration $i";
		res=$(./parte2 -grid $edge -profiling ./TestFiles/dolomiti30k);
		time=${res#* };
		time=${time%% *};
		if (( time < min ))
		then
			min=$time;
			ntr=${res%% *};
		fi
	done;
	## get memory usage
	res=$(./parte2 -grid $edge -memcheck ./TestFiles/$file);
	minsize=$(echo $res | awk '{print $2}');
	maxsize=$(echo $res | awk '{print $3}');
	avrgsize=$(echo $res | awk '{print $4}');
	echo "final result:"
	echo 'Edges          min_time    min_mem    max_mem   avrg_mem'
	printf ' %10d  %10d %10d %10d %10s\n' $edge $min $minsize $maxsize $avrgsize;
	echo "$edge $min $minsize $maxsize $avrgsize">>"./Results/results_dolomiti30k_grid_edges"
done;
