#include "MemoryTool.h"
#include <climits>
#include <cstdio>

#include <iostream>

MemoryTool::MemoryTool(void)
{
	reset();
}
unsigned long MemoryTool::getVirtMem(void) /// returns the virtual memory of the process in bytes
{
	struct proc_t usage;
	look_up_our_self(&usage);
	/*std::cout<<std::endl;
	std::cout<<usage.vm_lock<<" ";
	std::cout<<usage.vm_rss<<" ";
	std::cout<<usage.vm_data<<" ";
	std::cout<<usage.vm_stack<<" ";
	std::cout<<usage.vm_size<<" ";
	std::cout<<usage.vm_swap<<" ";
	std::cout<<usage.vm_exe<<" ";
	std::cout<<usage.vm_lib<<" ";
	std::cout<<usage.resident<<" ";
	std::cout<<usage.dt<<" ";
	std::cout<<usage.vsize<<std::endl;*/
	return usage.vsize;
}
unsigned long MemoryTool::getMemDirtyMode(void) /// returns the memory calculated in dirty mode of the process in kbytes
{
	char command[100];
	sprintf(command, "pmap -x %d | tail -1 | awk '{print $5}'", getpid());
	FILE* dmemf = popen(command,"r");
	unsigned long dmem;
	fscanf(dmemf,"%ld", &dmem);
	pclose(dmemf);
	return dmem;
}
unsigned long MemoryTool::checkVirtMem(void) /// returns the virtual memory of the process in bytes and updates min, max and average
{
	unsigned long vm = getVirtMem();
	if (vm<zero_vm)
	{
		vm = 0;
	} else
	{
		vm = vm - zero_vm;
	}
	if (vm<min_vm)
	{
		min_vm = vm;
	}
	if (vm>max_vm)
	{
		max_vm = vm;
	}
	average_vm = (long int)average_vm + ((long int)vm-(long int)average_vm)/(++count_vm);
}
unsigned long MemoryTool::getMinVirtMem(void)
{
	return min_vm;
}
unsigned long MemoryTool::getMaxVirtMem(void)
{
	return max_vm;
}
unsigned long MemoryTool::getAvrgVirtMem(void)
{
	return average_vm;
}
unsigned long MemoryTool::checkMemDirty(void) /// returns the memory calculated in dirty mode of the process in kbytes and updates min, max and average
{
	unsigned long drt = getMemDirtyMode();
	if (drt<zero_drt)
	{
		drt = 0;
	} else
	{
		drt = drt - zero_drt;
	}
	if (drt<min_drt)
	{
		min_drt = drt;
	}
	if (drt>max_drt)
	{
		max_drt = drt;
	}
	average_drt = (long double)average_drt + ((long double)drt-(long double)average_drt)/(long double)(++count_drt);
}
unsigned long MemoryTool::getMinMemDirty(void)
{
	return min_drt;
}
unsigned long MemoryTool::getMaxMemDirty(void)
{
	return max_drt;
}
long double MemoryTool::getAvrgMemDirty(void)
{
	return average_drt;
}
void MemoryTool::reset(void)
{
	count_vm = count_drt = 0;
	min_vm = min_drt = ULONG_MAX;
	max_vm = max_drt = 0;
	average_vm = 0;
	average_drt = 0.0;
	zero_drt = zero_vm = 0;
}
void MemoryTool::setZeroVM(unsigned long zero_vm)
{
	this->zero_vm = zero_vm;
}
void MemoryTool::setZeroDirty(unsigned long zero_drt)
{
	this->zero_drt = zero_drt;
}
