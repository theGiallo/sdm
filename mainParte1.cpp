#include "readGML.h"
#include "writeGML.h"
#include "libGeometry.h"
#include "GMLUtilities.h"
#include <iostream>
#include <sstream>

using namespace libGeometry;
using namespace GMLUtilities;

int main(int argc, char ** argv)
{
	if (argc==0) 
	{
		std::cout<<"Specificare nome del file GML senza estensione"<<std::endl;
		return 0;
	}
	else
	{
		std::cout<<"Sto per leggere file GML di nome "<<argv[1]<<":"<<std::endl;
		std::cout<<"  Lo schema da "<<argv[1]<<".xsd"<<std::endl;
		std::cout<<"  I dati da "<<argv[1]<<".gml"<<std::endl;
	}
	Polygon2D& pol = readPolygonFromGML(argv[1]);
	
#ifdef DEBUG
	Triangle2D t(Point2D(0,0),Point2D(3,3),Point2D(6,1));
	std::cout<<"orientation: "<<t.getNormalDir()<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(7,7))<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(8,1))<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(0,0))<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(1,1))<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(2,1))<<std::endl;
	t.changeOrientation(UP);
	std::cout<<"orientation: "<<t.getNormalDir()<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(7,7))<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(8,1))<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(0,0))<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(1,1))<<std::endl;
	std::cout<<t.whereIsPoint(Point2D(2,1))<<std::endl;
	
	std::cout<<std::endl<<(std::string)pol<<std::endl;
	std::cout<<(pol.getNormalDir()==UP?"UP":"DOWN")<<std::endl;
	pol.changeOrientation(pol.getNormalDir()==UP?DOWN:UP);
	std::cout<<(pol.getNormalDir()==UP?"UP":"DOWN")<<std::endl;
#endif
	std::cout<<(pol.getNormalDir()==UP?"UP":"DOWN")<<std::endl;
	pol.changeOrientation(UP);
	std::cout<<(pol.getNormalDir()==UP?"UP":"DOWN")<<std::endl;
	std::cout<<"--------------------------------"<<std::endl<<(std::string)pol<<std::endl<<std::endl;
	TrMesh2D_DumbPrint tmdp;
	std::cout<<"Triangolazione in corso..."<<std::endl;
	pol.triangulate(tmdp);
	TrMesh2D_TrBased tmtb;
	pol.triangulate(tmtb);
	std::cout<<"Triangolazione effettuata. ("<<tmtb.getTrianglesCount()<<" triangoli, "<<tmtb.getVerticesCount()<<" vertici)"<<std::endl;
	std::ostringstream oss;
	oss<<argv[1]<<"_triangolazione";
	writeTrianglesGML(tmtb, oss.str());

	oss.str("");
	oss<<argv[1]<<"_adiacenze";
	writeTrianglesAdjacentiesGML(tmtb, oss.str());

	exit(0);
}
