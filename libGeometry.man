libGeometry implementa diverse strutture dati utili per la manipolazione di dati geometrici.

Point2D rappresenta un punto nello spazio bidimensionale
Vec2 rappresenta un vettore nello spazio bidimensionale
Vec3 rappresenta un vettore nello spazio tridimensionale
Queste tre classi hanno implementate diverse operazioni, non esaustive per il loro tipo poichè sono state implementate solo le operazioni utili allo svolgimento dell aprima parte del progetto.

Edge2D rappresenta un edge costituitp da due punti nello spazio bidimensionale

Triangle2D rappresenta un triangolo nello spazio bidimensionale, è costituito da 3 Point2D. E' dotato di diversi metodi utili per conoscere la relazione posizionale rispetto ad un punto e per conoscere e modificare il suo orientamento.

Polygon2D rappresenta un poligono nello spazio bidimensionale. Presenta diversi costruttori, per dare la possibilità di aggiungere i punti uno ad uno, senza saperne la cardinalità finale, e per aggiungerli uno ad uno sapendo la cardinalità finale. Presenta due metodi per conoscerne e modificarne l'orientamento. E' presente un metodo per calcolare la triangolazione del poligono e memorizzarla in una struttura dati generica per triangolazioni 2D rappresentata dall'interfaccia ITrMesh2D.

L'interfaccia ITrMesh2D rappresenta l'interfaccia utile per una struttura dati generica per triangolazioni 2D.

La classe TrMesh2D_DumbPrint rappresenta un'implementazione finta di ITrMesh2D che  non memorizza i dati, ma si limita a stampare un messaggio ad ogni chiamata di funzione.

La classe TrMesh2D_TrBased rappresenta una triangolazione 2D memorizzata con una struttura indicizzata orientata ai triangoli. La classe non è completa, in quanto sono stati implementati soltanto i metodi necessari al completamento della prima parte del progetto.

L'algoritmo di triangolazione ha complessità temporale O(n^2), questa complessità è dovita al controllo della presenza di punti nel triangolo, che viene fatto su tutti i punti rimanenti nel poligono.
La complessità spaziale di Polygon2D è lineare (e circa di n*dimensionePoint2D) dopo l'inserimento o con l'inserimento a cardinalità nota.
La complessità spaziale di TrMesh2D_TrBased è lineare (e di circa n*(Point2D+int)+(n-2)*(6*int).

La finzione getTriangles(void) di TrMesh2D_TrBased non è stata ottimizzata per mancanza di tempo (presenta un'alta complessità spaziale).

E' stato aggiunto un puntatore a ITrMesh2D_TrBased_NewEntriesManager a TrMesh2D_TrBased. Se questo è NULL il controllo dell'esistenza di un punto viene fatto con una ricerca sequenziale nell'array dei vertici, e le adiacenze vengono calcolate solo con i suggerimenti, si potrebbe implementare il loro calcolo con i dati presenti; se presente invece viene interrogato prima dell'inserimento di un punto e al calcolo della relazione TT, fornendogli il punto come Point2D e l'id che avrebbe se venisse inserito, e l'edge come Point2D_int, contente gli id dei punti dell'edge, l'id del triangolo di cui si sta calcolando la relazione TT e l'id della relazione TT che si sta calcolando.
Sono presenti due implementazioni di ITrMesh2D_TrBased_NewEntriesManager: una utilizza una griglia per memorizzare i punti e una griglia triangolare per gli edge, l'altra utilizza due PR quad tree.








