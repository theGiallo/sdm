<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ AdiacenzeTriangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>100.000000</gml:X><gml:Y>0.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>150.000000</gml:X><gml:Y>160.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F0">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>123.333336,103.333336 116.666664,110.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F1">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>116.666664,110.000000 123.333336,103.333336 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F2">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>116.666664,110.000000 126.666664,116.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F3">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>126.666664,116.666664 116.666664,110.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>3</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F4">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>126.666664,116.666664 113.333336,123.333336 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>4</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F5">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>113.333336,123.333336 126.666664,116.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>5</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F6">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>113.333336,123.333336 113.333336,133.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>6</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F7">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>113.333336,133.333328 113.333336,123.333336 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>7</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F8">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>113.333336,133.333328 116.666664,143.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>8</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F9">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>116.666664,143.333328 113.333336,133.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>9</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F10">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>116.666664,143.333328 130.000000,143.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>10</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F11">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>130.000000,143.333328 116.666664,143.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>11</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F12">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>130.000000,143.333328 140.000000,136.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>12</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F13">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,136.666672 130.000000,143.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>13</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
