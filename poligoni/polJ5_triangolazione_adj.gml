<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ AdiacenzeTriangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>139.000000</gml:X><gml:Y>41.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>593.000000</gml:X><gml:Y>285.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F0">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>255.333328,166.000000 345.666656,165.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F1">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>345.666656,165.000000 255.333328,166.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F2">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>345.666656,165.000000 395.666656,229.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F3">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>395.666656,229.000000 345.666656,165.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>3</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F4">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>395.666656,229.000000 456.666656,245.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>4</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F5">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>456.666656,245.666672 395.666656,229.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>5</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F6">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>456.666656,245.666672 482.666656,176.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>6</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F7">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>482.666656,176.000000 456.666656,245.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>7</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
