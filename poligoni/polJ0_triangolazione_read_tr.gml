<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ Triangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>83.000000</gml:X><gml:Y>53.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>409.000000</gml:X><gml:Y>356.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:Triangolazione fid="F0">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>157.000000,297.000000 371.000000,64.000000 409.000000,356.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F1">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>157.000000,297.000000 409.000000,356.000000 98.000000,352.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F2">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>83.000000,53.000000 157.000000,297.000000 98.000000,352.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
