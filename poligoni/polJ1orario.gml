<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ polJ1orario.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>0.000000</gml:X><gml:Y>0.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>10.000000</gml:X><gml:Y>10.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:polJ1orario fid="F1">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>100.000000,60.000000 50.000000,70.000000 60.000000,110.000000 90.000000,70.000000 110.000000,130.000000 150.000000,90.000000 170.000000,50.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
    </ogr:polJ1orario>
  </gml:featureMember>
</ogr:FeatureCollection>
