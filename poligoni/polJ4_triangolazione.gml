<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ Triangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>100.000000</gml:X><gml:Y>100.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>230.000000</gml:X><gml:Y>260.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:Triangolazione fid="F0">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>160.000000,120.000000 170.000000,100.000000 190.000000,120.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F1">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>130.000000,130.000000 160.000000,120.000000 190.000000,120.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F2">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>200.000000,100.000000 230.000000,210.000000 200.000000,130.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F3">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>190.000000,120.000000 200.000000,100.000000 200.000000,130.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>3</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F4">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>190.000000,120.000000 200.000000,130.000000 180.000000,140.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>4</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F5">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>190.000000,170.000000 200.000000,150.000000 210.000000,210.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>5</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F6">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>190.000000,170.000000 210.000000,210.000000 180.000000,180.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>6</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F7">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,140.000000 190.000000,170.000000 180.000000,180.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>7</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F8">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,180.000000 190.000000,200.000000 200.000000,240.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>8</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F9">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>200.000000,240.000000 210.000000,230.000000 190.000000,260.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>9</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F10">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,180.000000 200.000000,240.000000 190.000000,260.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>10</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F11">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,180.000000 190.000000,260.000000 180.000000,210.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>11</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F12">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,180.000000 180.000000,210.000000 170.000000,240.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>12</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F13">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,140.000000 180.000000,180.000000 170.000000,240.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>13</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F14">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>170.000000,240.000000 160.000000,190.000000 170.000000,180.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>14</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F15">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,140.000000 170.000000,240.000000 170.000000,180.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>15</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F16">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,140.000000 170.000000,180.000000 170.000000,170.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>16</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F17">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>180.000000,140.000000 170.000000,170.000000 150.000000,130.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>17</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F18">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>190.000000,120.000000 180.000000,140.000000 150.000000,130.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>18</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F19">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>130.000000,130.000000 190.000000,120.000000 150.000000,130.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>19</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F20">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>150.000000,150.000000 160.000000,160.000000 150.000000,180.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>20</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F21">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>150.000000,180.000000 160.000000,230.000000 140.000000,190.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>21</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F22">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>150.000000,150.000000 150.000000,180.000000 140.000000,190.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>22</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F23">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>150.000000,130.000000 150.000000,150.000000 140.000000,190.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>23</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F24">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>150.000000,130.000000 140.000000,190.000000 130.000000,210.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>24</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F25">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>130.000000,210.000000 110.000000,180.000000 130.000000,180.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>25</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F26">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>130.000000,210.000000 130.000000,180.000000 140.000000,160.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>26</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F27">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>150.000000,130.000000 130.000000,210.000000 140.000000,160.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>27</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F28">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>150.000000,130.000000 140.000000,160.000000 140.000000,140.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>28</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F29">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>130.000000,130.000000 150.000000,130.000000 140.000000,140.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>29</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F30">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>140.000000,140.000000 130.000000,150.000000 130.000000,140.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>30</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F31">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>130.000000,130.000000 140.000000,140.000000 130.000000,140.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>31</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F32">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>100.000000,100.000000 130.000000,130.000000 130.000000,140.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>32</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
