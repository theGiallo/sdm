<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ Triangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>139.000000</gml:X><gml:Y>41.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>593.000000</gml:X><gml:Y>285.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:Triangolazione fid="F0">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>376.000000,285.000000 139.000000,172.000000 251.000000,41.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F1">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>376.000000,285.000000 251.000000,41.000000 410.000000,169.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F2">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>376.000000,285.000000 410.000000,169.000000 401.000000,233.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F3">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>593.000000,219.000000 376.000000,285.000000 401.000000,233.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>3</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F4">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>454.000000,76.000000 593.000000,219.000000 401.000000,233.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>4</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
