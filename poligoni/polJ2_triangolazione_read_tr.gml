<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ Triangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>10.000000</gml:X><gml:Y>40.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>250.000000</gml:X><gml:Y>200.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:Triangolazione fid="F0">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>120.000000,70.000000 50.000000,60.000000 130.000000,40.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F1">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>120.000000,70.000000 130.000000,40.000000 250.000000,40.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F2">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>10.000000,100.000000 120.000000,70.000000 250.000000,40.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F3">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>40.000000,120.000000 10.000000,100.000000 250.000000,40.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>3</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F4">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>10.000000,140.000000 40.000000,120.000000 250.000000,40.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>4</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F5">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>100.000000,150.000000 10.000000,140.000000 250.000000,40.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>5</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F6">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>100.000000,150.000000 250.000000,40.000000 140.000000,200.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>6</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Triangolazione fid="F7">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>20.000000,160.000000 100.000000,150.000000 140.000000,200.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>7</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
