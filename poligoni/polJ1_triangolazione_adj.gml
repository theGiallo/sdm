<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ AdiacenzeTriangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>50.000000</gml:X><gml:Y>50.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>170.000000</gml:X><gml:Y>130.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F0">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>143.333328,90.000000 126.666664,80.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F1">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>126.666664,80.000000 143.333328,90.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F2">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>126.666664,80.000000 100.000000,86.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F3">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>100.000000,86.666664 126.666664,80.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>3</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F4">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>100.000000,86.666664 80.000000,66.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>4</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F5">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>80.000000,66.666664 100.000000,86.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>5</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F6">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>80.000000,66.666664 66.666664,83.333336 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>6</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F7">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>66.666664,83.333336 80.000000,66.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>7</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
