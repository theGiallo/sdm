<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ AdiacenzeTriangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>64.000000</gml:X><gml:Y>0.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>409.000000</gml:X><gml:Y>356.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F0">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>312.333344,239.000000 221.333328,335.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F1">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>221.333328,335.000000 312.333344,239.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F2">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>221.333328,335.000000 112.666664,234.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F3">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>112.666664,234.000000 221.333328,335.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>3</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
