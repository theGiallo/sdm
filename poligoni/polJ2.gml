<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ polJ2.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>0.000000</gml:X><gml:Y>0.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>10.000000</gml:X><gml:Y>10.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:polJ2 fid="F1">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>10.000000,100.000000 120.000000,70.000000 50.000000,60.000000 130.000000,40.000000 250.000000,40.000000 140.000000,200.000000 20.000000,160.000000 100.000000,150.000000 10.000000,140.000000 40.000000,120.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
    </ogr:polJ2>
  </gml:featureMember>
</ogr:FeatureCollection>
