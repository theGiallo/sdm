<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ Triangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>160.000000</gml:X><gml:Y>100.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>190.000000</gml:X><gml:Y>120.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:Triangolazione fid="F0">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>160.000000,120.000000 170.000000,100.000000 190.000000,120.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Triangolo</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:Triangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
