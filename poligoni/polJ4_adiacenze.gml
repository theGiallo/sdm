<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ AdiacenzeTriangolazione.xsd"
          xmlns:ogr="http://ogr.maptools.org/"
          xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>100.000000</gml:X><gml:Y>100.000000</gml:Y></gml:coord>
      <gml:coord><gml:X>230.000000</gml:X><gml:Y>260.000000</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F0">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,113.333336 160.000000,123.333336 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>0</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F1">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>160.000000,123.333336 173.333328,113.333336 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>1</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F2">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>160.000000,123.333336 156.666672,126.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>2</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F3">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>210.000000,146.666672 196.666672,116.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>3</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F4">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>196.666672,116.666664 210.000000,146.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>4</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F5">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>196.666672,116.666664 190.000000,130.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>5</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F6">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>190.000000,130.000000 196.666672,116.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>6</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F7">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>190.000000,130.000000 173.333328,130.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>7</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F8">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>200.000000,176.666672 193.333328,186.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>8</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F9">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>193.333328,186.666672 200.000000,176.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>9</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F10">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>193.333328,186.666672 183.333328,163.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>10</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F11">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>183.333328,163.333328 193.333328,186.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>11</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F12">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>183.333328,163.333328 176.666672,186.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>12</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F13">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>190.000000,206.666672 190.000000,226.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>13</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F14">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>200.000000,243.333328 190.000000,226.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>14</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F15">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>190.000000,226.666672 190.000000,206.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>15</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F16">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>190.000000,226.666672 200.000000,243.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>16</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F17">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>190.000000,226.666672 183.333328,216.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>17</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F18">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>183.333328,216.666672 190.000000,226.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>18</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F19">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>183.333328,216.666672 176.666672,210.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>19</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F20">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>176.666672,210.000000 183.333328,216.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>20</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F21">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>176.666672,210.000000 176.666672,186.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>21</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F22">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>176.666672,186.666672 183.333328,163.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>22</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F23">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>176.666672,186.666672 176.666672,210.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>23</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F24">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>176.666672,186.666672 173.333328,186.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>24</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F25">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>166.666672,203.333328 173.333328,186.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>25</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F26">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,186.666672 176.666672,186.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>26</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F27">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,186.666672 166.666672,203.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>27</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F28">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,186.666672 173.333328,163.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>28</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F29">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,163.333328 173.333328,186.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>29</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F30">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,163.333328 166.666672,146.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>30</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F31">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>166.666672,146.666672 173.333328,163.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>31</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F32">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>166.666672,146.666672 173.333328,130.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>32</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F33">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,130.000000 190.000000,130.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>33</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F34">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,130.000000 166.666672,146.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>34</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F35">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>173.333328,130.000000 156.666672,126.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>35</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F36">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>156.666672,126.666664 160.000000,123.333336 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>36</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F37">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>156.666672,126.666664 173.333328,130.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>37</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F38">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>156.666672,126.666664 140.000000,133.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>38</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F39">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>153.333328,163.333328 146.666672,173.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>39</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F40">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>150.000000,200.000000 146.666672,173.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>40</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F41">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>146.666672,173.333328 153.333328,163.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>41</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F42">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>146.666672,173.333328 150.000000,200.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>42</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F43">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>146.666672,173.333328 146.666672,156.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>43</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F44">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>146.666672,156.666672 146.666672,173.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>44</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F45">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>146.666672,156.666672 140.000000,176.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>45</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F46">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,176.666672 146.666672,156.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>46</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F47">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,176.666672 140.000000,166.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>47</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F48">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>123.333336,190.000000 133.333328,183.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>48</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F49">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>133.333328,183.333328 123.333336,190.000000 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>49</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F50">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>133.333328,183.333328 140.000000,166.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>50</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F51">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,166.666672 140.000000,176.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>51</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F52">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,166.666672 133.333328,183.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>52</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F53">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,166.666672 143.333328,143.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>53</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F54">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>143.333328,143.333328 140.000000,166.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>54</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F55">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>143.333328,143.333328 140.000000,133.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>55</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F56">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,133.333328 156.666672,126.666664 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>56</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F57">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,133.333328 143.333328,143.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>57</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F58">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>140.000000,133.333328 133.333328,136.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>58</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F59">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>133.333328,143.333328 133.333328,136.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>59</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F60">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>133.333328,136.666672 140.000000,133.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>60</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F61">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>133.333328,136.666672 133.333328,143.333328 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>61</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F62">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>133.333328,136.666672 120.000000,123.333336 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>62</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:AdiacenzeTriangolazione fid="F63">
      <ogr:geometryProperty><gml:LineString><gml:coordinates>120.000000,123.333336 133.333328,136.666672 </gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:PID>63</ogr:PID>
      <ogr:PNAME>Adiacenza</ogr:PNAME>
      <ogr:PIMPORTANCE>0.400000</ogr:PIMPORTANCE>
    </ogr:AdiacenzeTriangolazione>
  </gml:featureMember>
</ogr:FeatureCollection>
