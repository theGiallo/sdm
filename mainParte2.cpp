#include "readGML.h"
#include "writeGML.h"
#include "libGeometry.h"
#include "GMLUtilities.h"
#include "TimeTool.h"
#include <iostream>
#include <sstream>
#include <cmath>
#include <cstring>
#include <stdlib.h>
#include "MemoryTool.h"

using namespace libGeometry;
using namespace GMLUtilities;

void printUsageAndExit(void)
{
	std::cout<<"Usage:"<<std::endl<<"	parte2 [[data structure type] [grid edge] [option]] file"<<std::endl<<std::endl<<"data structure type:"<<std::endl<<"	-list	uses a list data structure"<<std::endl<<"	-grid	uses a grid data structure with edge such that a cell contains log(num triangles) or as specified by user;"<<std::endl<<"	-qtree uses a PR QTree data structure"<<std::endl<<std::endl<<"grid edge: after -grid can be set the grid edge as an integer."<<std::endl<<std::endl<<"option:"<<std::endl<<std::endl<<"	-profiling	reads all the triangulation before inserting and prints only the inserting time;"<<std::endl<<"	-memcheck	prints only min, average and max memory usage in kB with zero point before allocating the triangulation and the auxiliary data structures."<<std::endl<<std::endl<<"	file: a GML file name without extension."<<std::endl<<std::endl;
	exit(1);
}

int main(int argc, char ** argv)
{
	MemoryTool vmem_checker;
	char* filepath, *arg_type, *arg_opt;
	bool profiling = false, memcheck = false;
	int grid_edge = -1;
	if (argc==1) 
	{
		printUsageAndExit();
	}
	else
	{
		if (argc==2)
		{
			filepath = argv[1];
			arg_type = "-grid";
			arg_opt = NULL;
		} else
		if (argc==3)
		{
			filepath = argv[2];
			arg_type = argv[1];
			arg_opt = NULL;
		} else
		if (argc==4)
		{
			filepath = argv[3];
			arg_type = argv[1];
			arg_opt = argv[2];
			if (strcmp(arg_opt, "-profiling")==0)
			{
				profiling = true;
			} else
			if (strcmp(arg_opt, "-memcheck")==0)
			{
				memcheck = true;
			} else
			{
				char cnum[50];
				int n = atoi(arg_opt);
				sprintf(cnum,"%d",n);
				if (strcmp(arg_opt, cnum)==0) /// check if opt is a int num
				{
					grid_edge = n;
				} else
				{
					printUsageAndExit();
				}
			}
		} else
		if (argc==5)
		{
			char *arg_opt2;
			filepath = argv[4];
			arg_type = argv[1];
			arg_opt = argv[2];
			arg_opt2 = argv[3];
			if (strcmp(arg_opt2, "-profiling")==0)
			{
				profiling = true;
			} else
			if (strcmp(arg_opt2, "-memcheck")==0)
			{
				memcheck = true;
			} else
			{
				printUsageAndExit();
			}
			char cnum[50];
			int n = atoi(arg_opt);
			sprintf(cnum,"%d",n);
			if (strcmp(arg_opt, cnum)==0) /// check if opt is a int num
			{
				grid_edge = n;
			} else
			{
				printUsageAndExit();
			}
		} else
		{
			printUsageAndExit();
		}
		
		if (!profiling && !memcheck)
		{
			std::cout<<"Sto per leggere file GML di nome "<<filepath<<":"<<std::endl;
			std::cout<<"  Lo schema da "<<filepath<<".xsd"<<std::endl;
			std::cout<<"  I dati da "<<filepath<<".gml"<<std::endl;
		}
	}
	/**
	 * Prepare structures
	 **/
	int nTrs = countEntityFeatures(filepath);
	int nVxs = nTrs*2;
	Vec2 bl,tr;
	char name[50];
	Triangle2D t;
	int n=0;
	int type = NO_TYPE;
	TimeTool timer;
	timer.stopAverageCount();
	ITrMesh2D_TrBased_NewEntriesManager* nem;
	if (!profiling)
	{
		timer.check(START_CHECK);
	}
	
	Triangle2D *tarray;
	if (profiling)
	{
		tarray = new Triangle2D[nTrs];
	}
	vmem_checker.setZeroDirty(vmem_checker.getMemDirtyMode());
	
	if (strcmp(arg_type, "-list")==0)
	{
		nem = new TrMesh2D_TrBased_GridNewEntriesManager(NULL, 1, 1, 1);
	} else
	if (strcmp(arg_type, "-grid")==0)
	{
		int l = grid_edge!=-1 ? grid_edge : ceil(sqrt(nVxs/log(nVxs)));
		int le = grid_edge!=-1 ? grid_edge : ceil((1+sqrt(1+(8*nVxs)/log(nVxs)))/2.0f);
		nem = new TrMesh2D_TrBased_GridNewEntriesManager(NULL, l, l, le);
	} else
	if (strcmp(arg_type, "-qtree")==0)
	{
		nem = new TrMesh2D_TrBased_PRQTreeNewEntriesManager(NULL);
	} else
	{
		printUsageAndExit();
	}
	
	openFile(std::string(filepath), name);
	readBoundaries(bl,tr);
	
	if (!profiling && !memcheck)
	{
		std::cout<<"Boundaries: "<<(std::string)bl<<" "<<(std::string)tr<<std::endl;
		std::cout<<"Leggo "<<nTrs<<" triangoli...";
	} else
	if (profiling)
	{
		char command[100];
		sprintf(command, "sudo renice -n -20 -p %d>/dev/null", getpid());
		system(command);
	}
	
	TrMesh2D_TrBased tmtb(nVxs, nem);
	tmtb.setExpectedBottomLeftBoundary(bl);
	tmtb.setExpectedTopRightBoundary(tr);
	nem->setOwner(&tmtb);
	int nt = 0;
/**
 * Read triangles from file
 **/
	while(readTriangle(t, &name[0], type))
	{
		if (!profiling) 
		{
			/**
			 * Add triangle to file
			 **/
			ITrMesh2D_Triangle& tmp = tmtb.addTriangle(t);
			delete &tmp;
			if (memcheck)
			{
				vmem_checker.checkMemDirty();
			}
		} else
		{
			tarray[nt] = t;
			nt++;
		}
	}
	closeFileAfterRead();

	if (profiling)
	{
		timer.check(START_CHECK);
		for (int i=0 ; i<nTrs ; i++)
		{
			/**
			 * Add triangle to file
			 **/
			ITrMesh2D_Triangle& tmp = tmtb.addTriangle(tarray[i]);
			delete &tmp;
		}
		timer.check(STOP_CHECK);
	}
	
	
#ifdef DEBUGPRQTREES
	std::cout<<std::endl<<"Vertices tree:"<<std::endl;
	dynamic_cast<TrMesh2D_TrBased_PRQTreeNewEntriesManager*>(nem)->printVerticesTree();
	std::cout<<std::endl<<"Edges tree:"<<std::endl;
	dynamic_cast<TrMesh2D_TrBased_PRQTreeNewEntriesManager*>(nem)->printEdgesTree();
#endif
	/**
	 * Print result
	 **/
	if (!profiling && !memcheck)
	{
		timer.check(MIDDLE_CHECK);
		std::cout<<"	fatto. ("<<timer.last_delta<<"μs)"<<std::endl;
		std::cout<<"Triangolazione letta. ("<<tmtb.getTrianglesCount()<<" triangoli, "<<tmtb.getVerticesCount()<<" vertici)"<<std::endl;
		std::cout<<"Salvo il file GML con la rappresentazione delle adiacenze...";
	} else
	if (profiling)
	{
		std::cout<<nTrs<<" "<<timer.last_delta<<std::endl;
	} else
	{
		std::cout<<nTrs<<" "<<vmem_checker.getMinMemDirty()<<" "<<vmem_checker.getMaxMemDirty()<<" "<<vmem_checker.getAvrgMemDirty()<<std::endl;
	}
	
	/**
	 * Create result files
	 **/
	delete nem;
	std::ostringstream oss;
	oss<<filepath<<"_read_tr";
	writeTrianglesGML(tmtb, oss.str());

	oss.str("");
	oss<<filepath<<"_adj";
	writeTrianglesAdjacentiesGML(tmtb, oss.str());
	
	if (!profiling && !memcheck)
	{
		std::cout<<"	fatto."<<std::endl;
		std::cout<<"Esco."<<std::endl;
	}
	exit(0);
}
