#include "GMLUtilities.h"
#include "readGML.h"
#include "writeGML.h"
#include <cstring>

using namespace libGeometry;
using namespace GMLUtilities;

/**
 * Read functions
 **/

Polygon2D& GMLUtilities::readPolygonFromGML(char* path)
{
	Polygon2D* pol = new Polygon2D();
	
	char name[50];
	int type, feature_type;
	int numAttrib;     /* length of array below */
	AttribInfo * info; /* array for attribute info */
	int n, i;
	double xp, yp;
	int nVert;
	int ok;
	
	numAttrib = countSchemaAttrib(path); 
	std::cout<<"MAIN SCHEMA: Ci sono "<<numAttrib<<" attributi nel file"<<std::endl;
	info = (AttribInfo*) malloc(numAttrib*sizeof(AttribInfo));

	openExistingSchemaFile(path);
	type = readSchemaHeading(name);
	printf("MAIN SCHEMA: Nome delle entita' = %s e tipo = %d\n",name,type);
	for (n=0; n<numAttrib; n++)
	{
		if (!readSchemaAttrib(&info[n]))
		{
			std::cout<<"MAIN SCHEMA: Errore lettura attributo n."<<n<<std::endl;
		}
    	std::cout<<"MAIN SCHEMA: Attributo n."<<n<<" ha nome "<<info[n].attr_name<<", tipo "<<info[n].attr_type<<", digits "<<info[n].attr_digits<<", decimals "<<info[n].attr_decimals<<std::endl;
	}
	closeExistingSchemaFile();
  
	if (!openFile(path, name))
	{
		throw false;
	}
	feature_type = NO_TYPE;
	if (type!=NO_TYPE)
	{  
		if (!readStartFeature(name, type))  
		{
			std::cout<<"MAIN ENTITA: Errore lettura feature"<<std::endl;
		}
		else
		{
			feature_type = type;
		}
	} else
	if (!readStartFeatureAndType(name, &feature_type))
	{
		std::cout<<"MAIN ENTITA: Errore lettura feature"<<std::endl;
	}
    std::cout<<"MAIN ENTITA: Leggo feature di tipo "<<feature_type<<std::endl;
	
	if (type==LINES) ok = readPolylineStart();
    ok = readPolygonStart();
    std::cout<<"MAIN ENTITA: Linea o poligono"<<std::endl;
    if (ok) std::cout<<"Ora leggo i vertici"<<std::endl;
    else std::cout<<"Errore"<<std::endl;
    nVert = 0;
    while (ok)
	{
		ok = readPolyVertex(&xp, &yp);
		if (ok) 
		{
		std::cout<<"MAIN ENTITA: Vert. n."<<nVert<<"  = ("<<xp<<","<<yp<<")"<<std::endl;
		pol->addVertex(Point2D(xp,yp));
		nVert++;
		}
    } 
    std::cout<<"Ho letto "<<nVert<<" vertici"<<std::endl;
	closeFileAfterRead();
	return (*pol);
}
bool GMLUtilities::openFile(std::string filename, char* out_features_name)
{
	openExistingEntityFile(const_cast<char *>(filename.c_str()));
	if (!readHeading(out_features_name))
	{
		std::cout<<"MAIN ENTITA': fallito readHeading"<<std::endl;
		return false;
	}
	return true;
}
void GMLUtilities::closeFileAfterRead(void)
{
	closeExistingEntityFile();
}
bool GMLUtilities::readStartFeatureWr(char* in_attrib_name, int* type)
{
	if (*type==NO_TYPE)
	{
		if (!readStartFeatureAndType(in_attrib_name, type))
		{
			return false;
		}
	} else
	{
		if (!readStartFeature(in_attrib_name, *type))
		{
			return false;
		}
	}
	return true;
}
bool GMLUtilities::readPolStart(char* in_attrib_name, int type)
{
	int ok;
	if (type==LINES)
	{
		ok = readPolylineStart();
	} else
    {
    	ok = readPolygonStart();
	}
    if (!ok)
    {
    	return false;
	}
	return true;
}
bool GMLUtilities::readTriangle(libGeometry::Triangle2D& out_tr, char* in_attrib_name, int type)
{
	int feature_type, ok;
	double xp, yp;
	readStartFeatureWr(in_attrib_name, &type);
	if (!readPolStart(in_attrib_name, type))
	{
		return false;
	}
    /*
    	It's orrible I know :[
    */
	if (readPolyVertex(&xp, &yp)) 
	{
		out_tr.setA(Point2D(xp,yp));
	} else
	{
		return false;
	}
	if (readPolyVertex(&xp, &yp)) 
	{
		out_tr.setB(Point2D(xp,yp));
	} else
	{
		return false;
	}
	if (readPolyVertex(&xp, &yp)) 
	{
		out_tr.setC(Point2D(xp,yp));
	} else
	{
		return false;
	}
	if (readPolyVertex(&xp, &yp))
	{
		std::cout<<"Warning: a Polygon with more than 3 vertices was read as Triangle"<<std::endl;
	}
    return true;
}
char* GMLUtilities::readAttribName(std::string filename, int n, char* out_name){
	int numAttrib = countSchemaAttrib(const_cast<char *>(filename.c_str()));
	if (n>=numAttrib)
	{
		return NULL;
	}
	AttribInfo info;
	char name[50];
	openExistingSchemaFile(const_cast<char *>(filename.c_str()));
	int type = readSchemaHeading(name);
	for (int i=0 ; i<=n ; i++)
	{
		if (!readSchemaAttrib(&info))
		{
			return NULL;
		}
	}
	strcpy(out_name, info.attr_name);
	closeExistingSchemaFile();
	return out_name;
}
void GMLUtilities::readBoundaries(libGeometry::Vec2& bl, libGeometry::Vec2& tr)
{
	double xmin, ymin, xmax, ymax;
	readBounds(&xmin, &ymin, &xmax, &ymax);
	bl.x = xmin;
	bl.y = ymin;
	tr.x = xmax;
	tr.y = ymax;
}

/**
 * Write functions
 **/
 
void GMLUtilities::writeSingleTriangle(Triangle2D t)
{
	writePolygonStart();
		writePolyVertex(t.getA().x, t.getA().y);
		writePolyVertex(t.getB().x, t.getB().y);
		writePolyVertex(t.getC().x, t.getC().y);
	writePolygonEnd();
}
void GMLUtilities::startFeature(std::string name, int id)
{
	writeStartFeature(const_cast<char *>(name.c_str()), id);
}
void GMLUtilities::endFeature(std::string name, std::string attr_name, int id)
{
	char pid[] = "PID";
	char pname[] = "PNAME";
	char pimportance[] = "PIMPORTANCE";
		writeIntAttrib(pid, id);
		writeStringAttrib(pname, const_cast<char *>(attr_name.c_str()));
		writeDecimalAttrib(pimportance, 0.4);
	writeEndFeature(const_cast<char *>(name.c_str()));
}
void GMLUtilities::openFile(std::string filename, std::string heading, Vec2 bl, Vec2 tr)
{
	openEntityFile(const_cast<char *>(filename.c_str())); 
		writeHeading(const_cast<char *>(heading.c_str()));
		writeBounds(bl.x, bl.y, tr.x, tr.y);
}
void GMLUtilities::closeFile()
{
		writeClosing();
	closeEntityFile();
}
void GMLUtilities::writeSchema(std::string filename, int entityType, std::string heading, std::string start)
{
	char pid[] = "PID";
	char pname[] = "PNAME";
	char pimportance[] = "PIMPORTANCE";
	openSchemaFile(const_cast<char *>(filename.c_str()));
	writeSchemaHeading(entityType, const_cast<char *>(heading.c_str()));
	writeSchemaGeneralStart(entityType, const_cast<char *>(start.c_str()));
	writeSchemaIntegerAttrib(pid, 4);
	writeSchemaStringAttrib(pname, 50);
	writeSchemaDecimalAttrib(pimportance, 10, 3);
	writeSchemaGeneralEnd();
	writeSchemaClosing();  
	closeSchemaFile();
}
void GMLUtilities::writeLine(Vec2 start, Vec2 end)
{
	writePolylineStart();
		writePolyVertex(start.x, start.y);
		writePolyVertex(end.x, end.y);
	writePolylineEnd();
}
void GMLUtilities::writeTrianglesGML(ITrMesh2D& trm, std::string filename)
{
	writeSchema(filename, POLYGONS, "Triangolazione", "Triangolazione");
	Vec2 bl = trm.getBottomLeftBoundary();
	Vec2 tr = trm.getTopRightBoundary();
	int i = 0;
	std::list<ITrMesh2D_Triangle*> tl = trm.getTriangles();
	openFile(filename, "Triangolazione", bl, tr);
		for (std::list<ITrMesh2D_Triangle*>::iterator it=tl.begin() ; it != tl.end() ; it++)
		{
		startFeature("Triangolazione", i);
			writeSingleTriangle((*it)->getTriangle());
			delete *it;
		endFeature("Triangolazione", "Triangolo", i++);
		}
	closeFile();
}
void GMLUtilities::writeTrianglesAdjacentiesGML(ITrMesh2D& trm, std::string filename)
{
	writeSchema(filename, LINES, "AdiacenzeTriangolazione", "AdiacenzeTriangolazione");
	Vec2 bl = trm.getBottomLeftBoundary();
	Vec2 tr = trm.getTopRightBoundary();
	int i = 0;
	std::list<ITrMesh2D_Triangle*> tl = trm.getTriangles();
	std::list<ITrMesh2D_Triangle*> tal;
	Vec2 start, end;
	openFile(filename, "AdiacenzeTriangolazione", bl, tr);
		for (std::list<ITrMesh2D_Triangle*>::iterator it=tl.begin() ; it != tl.end() ; it++)
		{
			tal = trm.getTriangles(**it);
			start = (*it)->getTriangle().getCentroid();
			for (std::list<ITrMesh2D_Triangle*>::iterator ita=tal.begin() ; ita != tal.end() ; ita++)
			{
				end = (*ita)->getTriangle().getCentroid();
		startFeature("AdiacenzeTriangolazione", i);
			writeLine(start,end);
			delete *ita;
		endFeature("AdiacenzeTriangolazione", "Adiacenza", i++);
			}
			delete *it;
		}
	closeFile();
}
