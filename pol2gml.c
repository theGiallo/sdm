
/*
Universita' degli Studi di Genova;
Corso di Laurea Magistrale in Informatica;
Corso di Spatial Data Management (SDM);
Docente Paola Magillo;
Utilita' per scrivere un file in formato GML.
*/

#include <string.h>
#include "writeGML.h"

/*
Da compilare con:
gcc writeGML.c pol2gml.c -o pol2gml
*/

/*********************************** MAIN **********************************/

int main(int argc, char **argv)
{
/* ------ legge poligono e scrive gml ------ */

  FILE * fd_in;
  int i,j;
  int num;
  double x, y;
  char out_name[255];
  
  if (argc<2)
  {
    fprintf(stderr,"Leggo da standard input\n");
    fd_in = stdin;
    /* inizializza nome output per default */
    strcpy(out_name,"polygon");
  }
  else
  {
    fprintf(stderr,"Leggo poligono da %s\n",argv[1]);
    fd_in = fopen(argv[1],"r");
    if (!fd_in)
    {
      fprintf(stderr,"Errore apertura file %s\n",argv[1]);
      return 0;
    }
    /* inizializza nome output con nome input tolta l'estensione */
    j = -1;
    for (i=0; (i<strlen(argv[1]) && (j<=-1)); i++)
    {
      if (argv[1][i]=='.') j = i;
    }
    strcpy(out_name, argv[1]);
    out_name[j] = '\0';
  }
  fprintf(stderr,"Scrivo file gml %s.xsd e %s.gml\n", out_name, out_name);
  
/* ------ poligoni ------ */

  /* scrive schema */
  openSchemaFile(out_name);
  writeSchemaHeading(POLYGONS, out_name);
  writeSchemaGeneralStart(POLYGONS, out_name);
  writeSchemaGeneralEnd();
  writeSchemaClosing();  
  closeSchemaFile();
  
  if (fd_in==stdin)  fprintf(stderr,"Numero vertici?\n");
  if (fscanf(fd_in,"%d", &num)!=1)
  {
    fprintf(stderr,"Errore lettura numero vertici\n");
    return 0;        
  }
  
  /* scrive contenuto, un poligono */
  openEntityFile(out_name);  
  writeHeading(out_name);
  writeBounds(0.0, 0.0, 10.0, 10.0);
  writeStartFeature(out_name, 1);
  writePolygonStart();
  for (i=0; i<num; i++)
  {
    if (fd_in==stdin)
      fprintf(stderr,"X Y vertice %d (separate da spazio)?\n", i);
    if (fscanf(fd_in, "%lf %lf", &x, &y)!=2)
    {
      fprintf(stderr,"Errore lettura vertice i\n",i);
      return 0;
    }
    writePolyVertex(x,y);
  }
  writePolygonEnd();
  writeEndFeature(out_name);
  writeClosing();
  closeEntityFile();
}
