/*
Universita' degli Studi di Genova;
Corso di Laurea Magistrale in Informatica;
Corso di Spatial Data Management (SDM);
Docente Paola Magillo;
Utilita' per scrivere un file in formato GML.
*/

#ifndef SDM_2012_READ_GML_H
#define SDM_2012_READ_GML_H

#include <stdio.h>

/********************************** MACRO **********************************/

/* Tipi possibili di geometria per un'entita' */
#define POINTS 0
#define LINES 1
#define POLYGONS 2
#define NO_TYPE 99

/* Tipi possibili per gli attributi: intero, decimale, stringa */
#define INT_ATTRIB 1
#define DEC_ATTRIB 2
#define STR_ATTRIB 3

/* Info su un attributo */
typedef struct AttribInfoStruct
{
  int attr_type;
  char * attr_name;
  int attr_digits; /* totale cifre, o caratteri per le stringhe */
  int attr_decimals; /* solo per attributi decimali */
} AttribInfo;

/**************************** GESTIONE DEI FILE ****************************/

/* Le due funzioni di apertura ritornano 1/0 (true/false) se il file
   esisteva e quindi e' stato aperto con successo oppure no */

extern int openExistingSchemaFile(char * fileName);
extern void closeExistingSchemaFile();

extern int openExistingEntityFile(char * fileName);
extern void closeExistingEntityFile();

/****************** CONTEGGIO ELEMENTI (A FILE CHIUSO)  ********************/

/* A file chiuso, conta quanti attributi sono definiti nel
   file di schema */
extern int countSchemaAttrib(char * fileName);

/* A file chiuso, conta quante feature sono presenti nel file di dati */
extern int countEntityFeatures(char * fileName);

/************************* LETTURA DEL FILE SCHEMA *************************/

/* Da chiamare per primo, ritorna il tipo di entita' presente nel file,
   se errore ritorna NO_TYPE, e inizializza il nome col nome del tipo
   di feature definite nello schema */
extern int readSchemaHeading(char * name);

/* Da chiamare per secondo, ritorna 1/0 (con significato true/false) se
   esiste un altro attributo, in caso esista riempie la tabella passata */
extern int readSchemaAttrib(AttribInfo * info);

/************************** LETTURA DEL FILE DATI **************************/

/* Tutte le funzioni ritornano 1/0 (true/false) se la lettura e' stata 
   eseguita con successo o fallimento */

/* Da chiamare per primo, inizializza il nome */
extern int readHeading(char * name);

/* Da chiamare per secondo, inizializza le 4 variabili passate con gli
   estremi minimi e massimi delle coordinate del dominio, letti da file  */
extern int readBounds(double * xmin, double * ymin, double * xmax, double * ymax);
  
/* da chiamare all'inizio di ogni feature, nel caso che il tipo della 
   feature sia noto; se non ci sono altre feature ritorna 0 (false), 
   se ritorna 1 (true) allora dopo si puo' leggere la feature */
extern int readStartFeature(char * name, int type);

/* da chiamare all'inizio di ogni feature, nel caso che il tipo della 
   feature non sia noto; inizializza il tipo; se non ci sono altre 
   feature ritorna 0 (false), se ritorna 1 (true) allora dopo si puo'
   leggere la feature */
extern int readStartFeatureAndType(char * name, int * type);

/* da chiamare per ogni feature, funzione che legge la geometria
   (ved. dopo), secondo il tipo previsto dallo schema */

/* da chiamare per ogni feature, funzioni che leggono gli attributi
   (ved. dopo), quelli previsti dallo schema */

/* da chiamare alla fine di ogni feature */
extern int readEndFeature(char * name);

/*************************** GEOMETRIA E ATTRIBUTI *************************/

/* legge geometria del punto */
extern int readPointGeom(double * x, double * y);

/* legge geometria della polyline, tutta assieme sapendo quanti vertici
   ci aspettiamo (es. sappiamo che il file contiene segmenti, e mette
   le coordinate nei due array, che devono essere stati gia' allocati */
extern int readPolylineGeom(int len, double * x, double * y);

/* come sopra, legge geometria del poligono tutta assieme, sapendo gia'   
   quanti saranno i vertici */
extern int readPolygonGeom(int len, double * x, double * y);

/* leggono geometria di polyline e poligono un pezzo per volta, queste
   funzioni leggono l'inizio */
extern int readPolylineStart();
extern int readPolygonStart();

/* per geometria polyline o poligono, legge un vertice, ritorna 0
  (false) se non ci sono altri vertici */
extern int readPolyVertex(double * x, double * y);

/* per geometria polyline o poligono, leggono la fine */
extern int readPolylineEnd();
extern int readPolygonEnd();

/* legge attributo intero avente il nome passato, il valore viene
   inizializzato con quello letto */
extern int readIntAttrib(char * name, int * value);

/* legge attributo stringa avente il nome passato, il valore viene
   inizializzato con quello letto */
extern int readStringAttrib(char * name, char * value);

/* legge attributo numero decimale avente il nome passato, il valore
   viene inizializzato con quello letto */
extern int readDecimalAttrib(char * name, double * value);

#endif /* SDM_2012_READ_GML_H */
