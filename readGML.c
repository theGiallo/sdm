/*
Universita' degli Studi di Genova;
Corso di Laurea Magistrale in Informatica;
Corso di Spatial Data Management (SDM);
Docente Paola Magillo;
Utilita' per scrivere un file in formato GML.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "readGML.h"

/*************************** VARIABILI AUSILIARIE **************************/

/* File di input */
FILE * fd_schema_in;
FILE * fd_data_in;

#define MY_MAX_LEN 6000

/* Tiene l'ultimo tag letto, cioe' una parte di file delimitata da <>,
   viene inizializzato dalla funzione readTag */
static char lastTag[500] = "";

/* tiene l'ultima parte di file letta che sta tra due tag aperto/chiuso
   corrispondenti, viene inizializzato dalla funzione readInsideTag */
static char lastFragment[MY_MAX_LEN] = "";

/* tiene tutto cio' che e' stato letto e scartato per arrivare da dove
   eravamo fino all'inizio dell'ultimo tag letto in lastTag,
   viene inizializzato dalla funzione readTag */
static char lastSkip[MY_MAX_LEN] = "";

int keepFragment = 0;

/*************************** FUNZIONI AUSILIARIE ***************************/

/* Legge da file fino ad incontrare un inizio tag (carattere '<'),
   mette in lastSkip quello che ha saltato,
   scorre tutta la parte di file delimitata fra '<' e '>', mette
   in lastTag la stringa contenuta entro tali delimitatori inclusi */
   
/***header aggiunto da Gianluca Alloisio per compilazione con g++***/
int searchString(char * aString, char * key);
   
int readTag(FILE *fd)
{
  char c = '\0';
  char aux[500] = "";
  int len = 0;
  int k = 0;
  while ( (fscanf(fd, "%c", &c)==1) && (c!='<') && (c!=EOF) )
  {  if (keepFragment && (c!=EOF)) lastSkip[k++] = c;  }
  lastSkip[k] = '\0';
  if (c==EOF) return 0;
  aux[len++] = '<';
  while ( (fscanf(fd, "%c", &c)==1) && (c!='>') && (c!=EOF) )
  {  aux[len++] = c;  }
  if (c!='>') return 0;
  if (len==0) return 0;
  aux[len++] = '>';
  aux[len] = '\0';
  strcpy(lastTag,aux);
  return 1;
}

/* Continua a leggere tag dal file finche' non trova uno che contenga
   la parola chiave, ritorna 1 se trovato, 0 altrimenti */
int readTagUntil(FILE *fd, char * key)
{
  int found = 0;
  while ( !found && readTag(fd) )
  {  found = searchString(lastTag, key);  }
  return found;
}

/* Legge una parte di file delimitata fra le due parole chiave,
   ritorna in lastFragment la stringa, esclusi i  delimitatori */
int readInsideTag(FILE *fd, char * key)
{
  char aux1[50] = "<";
  char aux2[50] = "</";
  strcat(aux1,key);
  strcat(aux2,key);
  strcat(aux1,">");
  strcat(aux2,">");
  
  int found = 0;
  while ( !found && readTag(fd) )
  {  found = searchString(lastTag, aux1);  }
  if (!found) return 0;
  found = 0;
  while ( !found && readTag(fd) )
  {  found = searchString(lastTag, aux2);  }
  if (!found) return 0;
  if (keepFragment) strcpy(lastFragment, lastSkip);
  return 1;
}

/* Cerca la stringa key dentro la stringa aString */
int searchString(char * aString, char * key)
{
  int i = 0;
  /* find keyword key within aString */
  while (strlen(key)+i <= strlen(aString))
  {
    if ( strncasecmp(aString+i, key, strlen(key))==0 ) break;
    else i++;
  }
  if (strlen(key)+i>strlen(aString))  return 0;
  return 1;
}

/* Come sopra ma in caso trovato ritorna l'indice in cui
   inizia la parola chiave, se non trovato ritorna -1 */
int searchStringIndex(char * aString, char * key)
{
  int i = 0;
  /* find keyword key within aString */
  while (strlen(key)+i <= strlen(aString))
  {
    if ( strncasecmp(aString+i, key, strlen(key))==0 ) break;
    else i++;
  }
  if (strlen(key)+i>strlen(aString))  return -1;
  return i;
}

/* Cerca la stringa key dentro la stringa aString, e 
in caso la trovi, cerca in aString una successiva delimitata 
tra "" e la memorizza in val */
int searchStringValue(char * aString, char * key, char * val)
{
  int i = 0;
  /* find keyword key within aString */
  while (strlen(key)+i <= strlen(aString))
  {
    if ( strncasecmp(aString+i, key, strlen(key))==0 ) break;
    else i++;
  }
  if (strlen(key)+i>strlen(aString))
  {  return 0; }
  
  /* take string between "" */
  int j = 0;
  while (aString[i]!='\"')
  {
    i++;
    if (i>=strlen(aString)) return 0;
  }
  i++;
  while (aString[i]!='\"') 
  {
    val[j++] = aString[i];
    i++;
    if (i>=strlen(aString)) return 0;
  }
  val[j] = '\0';
  return 1;
}

int searchValueBetweenTags(char * aString, char * key1, char *key2, char * val)
{
  int i1 = 0, i2 = 0;
  
  /* find keyword key1 within aString */
  while (strlen(key1)+i1 <= strlen(aString))
  {
    if ( strncasecmp(aString+i1, key1, strlen(key1))==0 ) break;
    else i1++;
  }
  if (strlen(key1)+i1>strlen(aString)) return 0;

  /* find keyword key2 within aString */
  while (strlen(key2)+i2 <= strlen(aString))
  {
    if ( strncasecmp(aString+i2, key2, strlen(key2))==0 ) break;
    else i2++;
  }
  if (strlen(key2)+i2>strlen(aString)) return 0;
  
  /* take part between them */
  int j = 0;
  while ( (i1+j)<i2 )
  {  val[j++] = aString[i1+j];  }
  val[j] = '\0';
  return 1;
}

/* La stringa data contiene piu' numeri, separati da spazi o da virgole,
   legge il primo numero e accorcia la stringa togliendo la parte letta,
   ritorna 1/0 se tutto ok o errore */
int getNextNumber(char * aString, double *r)
{
  char aux[MY_MAX_LEN];
  int i = 0;
  while ( (aString[i]>='0' && aString[i]<='9') || 
          (aString[i]=='.') ||
          (aString[i]=='-') ||
          (aString[i]=='e') || (aString[i]=='E') )
  {  i++;  }
  aString[i] = '\0';
  if (sscanf(aString,"%lf", r)!=1) return 0;
  strcpy(aux,aString+i+1);
  strcpy(aString,aux);
  return 1;
}

/**************************** GESTIONE DEI FILE ****************************/

/* Le due funzioni di apertura ritornano 1/0 (true/false) se il file
   esisteva e quindi e' stato aperto con successo oppure no */

int openExistingSchemaFile(char * fileName)
{
  char aux[50];
  strcpy(aux,fileName);
  strcat(aux,".xsd");
  fd_schema_in = fopen(aux,"r");
  return (fd_schema_in != NULL);
}
 
void closeExistingSchemaFile() {  fclose(fd_schema_in);  }

int openExistingEntityFile(char * fileName)
{
  char aux[50];
  strcpy(aux,fileName);
  strcat(aux,".gml");
  fd_data_in = fopen(aux,"r");
  return (fd_data_in != NULL);
}

void closeExistingEntityFile() {  fclose(fd_data_in);  }

/****************** CONTEGGIO ELEMENTI (A FILE CHIUSO)  ********************/

/* A file chiuso, conta quanti attributi sono definiti nel
   file di schema */
int countSchemaAttrib(char * fileName)
{
  int count = 0;
  int found = 0;
  char aux[255];
  openExistingSchemaFile(fileName);
  while ( !found && readTag(fd_schema_in) )
  {
    found = searchStringValue(lastTag, "xs:element name", aux) && 
            searchString(lastTag, "nillable") && 
            searchString(lastTag, "minOccurs") && 
            searchString(lastTag, "maxOccurs");
    /* do not count geometry attribute */
    found = found && (strcasecmp(aux,"geometryProperty")!=0);
    if (found) count++;
    found = 0;
  }
  closeExistingSchemaFile();
  return count;  
}

/* A file chiuso, conta quante feature sono presenti nel file di dati */
int countEntityFeatures(char * fileName)
{
  int count = 0;
  int found = 0;
  openExistingEntityFile(fileName);  
  while ( !found && readTagUntil(fd_data_in, "gml:featureMember") )
  {
    found = readTagUntil(fd_data_in, "/gml:featureMember");
    if (found) count++;
    found = 0;
  }
  closeExistingEntityFile();
  return count;
}

/************************* LETTURA DEL FILE SCHEMA *************************/

/* Da chiamare per primo, ritorna il tipo di entita' presente nel file,
   se errore ritorna NO_TYPE, e inizializza il nome col nome del tipo
   di feature definite nello schema */
int readSchemaHeading(char * name)
{
  char aux[255];
  int found = 0;
  while ( !found && readTag(fd_schema_in) )
  {
    found = searchStringValue(lastTag, "xs:element name", name);
    if (found && searchStringValue(lastTag, "type", aux)) 
    {
      if (!searchString(aux,name)) found = 0;
      if (found && searchStringValue(lastTag, "substitutionGroup", aux));
      if (found) found = (strcasecmp(aux,"gml:_Feature")==0);
    }
    else found = 0;
  }
  if (!found) return NO_TYPE;

  found = 0;
  while ( !found && readTag(fd_schema_in) )
  {
    found = searchStringValue(lastTag, "xs:element name", aux);
    if (found)        
    {
      if (strcasecmp(aux,"geometryProperty")==0)
      {
        found = searchStringValue(lastTag, "type", aux);
        if (found)
        {
          if (searchString(aux,"PointPropertyType")) return POINTS;
          else if (searchString(aux,"LineStringPropertyType")) return LINES;
          else if (searchString(aux,"PolygonPropertyType")) return POLYGONS;
        }
      }
    }
  }
  return NO_TYPE;
}

/* Da chiamare per secondo, ritorna 1/0 (con significato true/false) se
   esiste un altro attributo, in caso esista riempie la tabella passata */
int readSchemaAttrib(AttribInfo * info)
{   
  char aux[255];
  int found = 0;
  while ( !found && readTag(fd_schema_in) )
  {
    found = searchStringValue(lastTag, "xs:element name", aux) && 
            searchString(lastTag, "nillable") && 
            searchString(lastTag, "minOccurs") && 
            searchString(lastTag, "maxOccurs");

    if (found) // nome attributo trovato, cerco le altre info
    {
      info->attr_name = malloc((1+strlen(aux))*sizeof(char));
      strcpy(info->attr_name, aux);
      
      found = 0;
      while ( !found && readTag(fd_schema_in) )
      {
        found = searchStringValue(lastTag,"xs:restriction base",aux);
        if (found) 
        {
          if (strcasecmp(aux,"xs:integer")==0) info->attr_type = INT_ATTRIB;
          else if (strcasecmp(aux,"xs:string")==0) info->attr_type = STR_ATTRIB;
          else if (strcasecmp(aux,"xs:decimal")==0) info->attr_type = DEC_ATTRIB;
        }
      }
      found = 0;
      while ( !found && readTag(fd_schema_in) )
      {
        info->attr_digits = 0;
        info->attr_decimals = 0;
        if (info->attr_type==INT_ATTRIB) 
        {
          found = searchStringValue(lastTag, "xs:totalDigits value", aux);
          if (found) sscanf(aux,"%d", &info->attr_digits);
        }
        else if (info->attr_type==STR_ATTRIB)
        {
          found = searchStringValue(lastTag, "xs:maxLength value", aux);
          if (found) sscanf(aux,"%d", &info->attr_digits);
        }  
        else if (info->attr_type==DEC_ATTRIB)
        {
          found = searchStringValue(lastTag, "xs:totalDigits value", aux);
          if (found) sscanf(aux,"%d", &info->attr_digits);
          found = searchStringValue(lastTag, "xs:fractionDigits value", aux); 
          if (found) sscanf(aux,"%d", &info->attr_decimals);
        }
        return 1;
      }   
    }
  } // end while, not found
  return 0;
}

/************************** LETTURA DEL FILE DATI **************************/

/* Tutte le funzioni ritornano 1/0 (true/false) se la lettura e' stata 
   eseguita con successo o fallimento */

/* Da chiamare per primo, inizializza il nome */
int readHeading(char * name)
{
  char aux[255];
  int found = 0;
  while ( !found && readTag(fd_data_in) )
  {
    found = searchString(lastTag, "ogr:FeatureCollection") &&
            searchStringValue(lastTag, "xsi:schemaLocation", aux);
    if (found)
    {
      /* assegno i1 = posizione successiva all'ultima occorrenza di "/"
         e i2 = occorrenza di ".xsd" */
      int i1 = 0, i2 = 0, temp = 0; 
      while (temp != -1)
      {
        temp = searchStringIndex(aux+i1,"/");
        if (temp!=-1) i1 += (temp+1);
      }
      /* salto eventuale spazio bianco dopo il "/" */
      if (aux[i1]==' ') i1++;
      i2 = i1 + 1 + searchStringIndex(aux+i1+1,".xsd");
      if ((i1==-1)||(i2==-1)) return 0;
      strncpy(name, (aux+i1), (i2-i1));
      name[i2-i1] = '\0';
    }
  }
  return found;
}

/* Da chiamare per secondoo, inizializza le 4 variabili passate con gli 
   estremi minimi e massimi delle coordinate del dominio, letti da file */
int readBounds(double * xmin, double * ymin, double * xmax, double * ymax)
{
  char aux[255];
  char aux2[50];
  int found = 0;
  found = readTagUntil(fd_data_in, "gml:boundedBy");
  if (!found) return 0;
  found = readTagUntil(fd_data_in, "gml:Box");
  if (!found) return 0;

  keepFragment = 1;
  found = readInsideTag(fd_data_in, "gml:X");
  if (found) sscanf(lastFragment,"%lf",xmin);
  else printf(" non trovato xmin\n");
  found = readInsideTag(fd_data_in, "gml:Y");
  if (found) sscanf(lastFragment,"%lf",ymin);
  found = readInsideTag(fd_data_in, "gml:X");
  if (found) sscanf(lastFragment,"%lf",xmax);
  found = readInsideTag(fd_data_in, "gml:Y");
  if (found) sscanf(lastFragment,"%lf",ymax);
  keepFragment = 0;
  return (found);
}
  
/* da chiamare all'inizio di ogni feature, nel caso che il tipo della 
   feature sia noto; se non ci sono altre feature ritorna 0 (false), 
   se ritorna 1 (true) allora dopo si puo' leggere la feature */
int readStartFeature(char * name, int type)
{ 
  char aux[255] = "ogr:";
  strcat(aux,name); //adesso aux = ogr:name
  readTagUntil(fd_data_in, "gml:featureMember");
  readTagUntil(fd_data_in, aux);
  switch (type)
  {  case POINTS: return readTagUntil(fd_data_in, "gml:Point");
     case LINES: return readTagUntil(fd_data_in, "gml:LineString");
     case POLYGONS: return readTagUntil(fd_data_in, "gml:Polygon");
  }
  return 0;
}

/* da chiamare all'inizio di ogni feature, nel caso che il tipo della 
   feature non sia noto; inizializza il tipo; se non ci sono altre
   feature ritorna 0 (false), se ritorna 1 (true) allora dopo si puo'
   leggere la feature  */
int readStartFeatureAndType(char * name, int * type)
{ 
  char aux[255] = "ogr:";
  strcat(aux,name); //adesso aux = ogr:name
  readTagUntil(fd_data_in, "gml:featureMember");
  readTagUntil(fd_data_in, aux);
  readTagUntil(fd_data_in, "ogr:geometryProperty");
  readTag(fd_data_in);
  if (strcasecmp(lastTag,"<gml:Point>")==0)
  {  (*type) = POINTS;  }
  else if (strcasecmp(lastTag,"<gml:LineString>")==0)
  {  (*type) = LINES;  }
  else if (strcasecmp(lastTag,"<gml:Polygon>")==0)
  {  (*type) = POLYGONS;  }
  else return 0;
  return 1;
}

/* da chiamare per ogni feature, funzione che legge la geometria
   (ved. dopo), secondo il tipo previsto dallo schema */
   
/* da chiamare per ogni feature, funzioni che leggono gli attributi    
   (ved. dopo), quelli previsti dallo schema */

/* da chiamare alla fine di ogni feature */
int readEndFeature(char * name)
{  return readTagUntil(fd_data_in, "/gml:featureMember");  }

/*************************** GEOMETRIA E ATTRIBUTI *************************/

/* legge geometria del punto */
int readPointGeom(double * x, double * y)
{
  keepFragment = 1;
  readInsideTag(fd_data_in, "gml:coordinates");
  keepFragment = 0;
  if (!getNextNumber(lastFragment, x)) return 0;
  if (!getNextNumber(lastFragment, y)) return 0;
  return 1;
}

/* legge geometria della polyline, tutta assieme sapendo quanti vertici
   ci aspettiamo (es. sappiamo che il file contiene segmenti, e mette  
   le coordinate nei due array, che devono essere stati gia' allocati */
int readPolylineGeom(int len, double * x, double * y)
{
  int i;
  if (readPolylineStart() != len) return 0;
  for (i=0; i<len; i++)
  {
    if (!readPolyVertex(&x[i],&y[i])) return 0;
  }
  return 1;
}

/* come sopra, legge geometria del poligono tutta assieme, sapendo gia'
   quanti saranno i vertici */
int readPolygonGeom(int len, double * x, double * y)
{  return readPolylineGeom(len, x, y);  }

/* geometria polyline e poligono inizio, ritornano 1 se successo, 0
   se fallimento */
/* leggono geometria di polyline e poligono un pezzo per volta, queste
   funzioni leggono l'inizio */
int readPolylineStart()
{
  return readTagUntil(fd_data_in, "gml:coordinates");
}

int readPolygonStart() {  return readPolylineStart();  }

/* per geometria polyline o poligono, legge un vertice, ritorna 0          
  (false) se non ci sono altri vertici */
int readPolyVertex(double * x, double * y)
{
  char c;
  if (fscanf(fd_data_in, "%lf", x)!=1) return 0;
  while ( (fscanf(fd_data_in, "%c", &c)==1) && (c!=',') );
  if (fscanf(fd_data_in, "%lf", y)!=1) return 0;
  while ( (fscanf(fd_data_in, "%c", &c)==1) && (c!=' ') && (c!='<') );
  return 1;
}

/* per geometria polyline o poligono, leggono la fine */
int readPolylineEnd() {   return 1;  }
int readPolygonEnd()  {   return 1;  }

/* legge attributo intero avente il nome passato, il valore viene
   inizializzato con quello letto */
int readIntAttrib(char * name, int * value)
{
  int res;
  char aux[255] = "ogr:";
  strcat(aux,name); //adesso "ogr:name"
  keepFragment = 1;
  res = readInsideTag(fd_data_in, aux);
  keepFragment = 0;
  return res && (sscanf(lastFragment,"%d", value)==1);
}

/* legge attributo stringa avente il nome passato, il valore viene
   inizializzato con quello letto */
int readStringAttrib(char * name, char * value)
{
  int res;
  char aux[255] = "ogr:";
  strcat(aux,name); //adesso aux = ogr:name
  keepFragment = 1;
  res = readInsideTag(fd_data_in, aux);
  keepFragment = 0;
  if (res) strcpy(value,lastFragment);
  return res;
}

/* legge attributo numero decimale avente il nome passato, il valore viene
   inizializzato con quello letto */
int readDecimalAttrib(char * name, double * value)
{
  int res;
  char aux[255] = "ogr:";   
  strcat(aux,name); //adesso aux = ogr:name
  keepFragment = 1;
  res = readInsideTag(fd_data_in, aux);
  keepFragment = 0;
  return res && (sscanf(lastFragment,"%lf", value)==1);
}
