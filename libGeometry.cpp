#include "libGeometry.h"
#include <cmath>
#include <iostream>
#include <sstream>
#include <cstring>
#include <cfloat>

using namespace libGeometry;

// namespace functions
bool libGeometry::equalWithinEpsilon(float v1, float v2, float eps)
{
	return abs(v1-v2)<=abs(eps);
}

//class Point2D
Point2D::Point2D()
{
	x=y=0.0f;
}
Point2D::Point2D(float x, float y)
{
	this->x=x;
	this->y=y;
}
Point2D::operator Vec2() const
{
	return Vec2(x,y);
}
Point2D::operator std::string(void) const
{
	std::ostringstream oss;
	oss<<"("<<x<<", "<<y<<")";
	return oss.str();
}
bool Point2D::operator == (Point2D p) const
{
	return x==p.x && y==p.y;
}		
float Point2D::getAngle(Point2D a, Point2D b, Point2D c) /// returns the angle abcexpressed in radians
{
	return Vec2(b,c).getAngle(Vec2(b,a));
}
float Point2D::getSin(Point2D a, Point2D b, Point2D c)
{
    return Vec2(b,c).getSin(Vec2(b,a));
}
float Point2D::getCos(Point2D a, Point2D b, Point2D c)
{
	return Vec2(b,c).getCos(Vec2(b,a));
}
float Point2D::getNormalModule(Point2D a, Point2D b, Point2D c)
{
	return getSin(a, b, c);
}
NormalDir Point2D::getNormalDir(Point2D a, Point2D b, Point2D c)
{
	float s = Point2D::getSin(a,b,c);
	if (s>-SIN_EPS && s<SIN_EPS)
	{
		s=0;
		return NONE;
	}
	return s>0?UP:DOWN;
}

//	class Point2D_int
Point2D_int::Point2D_int(void)
{
	x=y=0;
}
Point2D_int::Point2D_int(int x, int y)
{
	this->x = x;
	this->y = y;
}
Point2D_int::operator Point2D(void) const
{
	return Point2D(x,y);
}
bool Point2D_int::operator == (Point2D_int p) const
{
	return x==p.x&&y==p.y;
}
Point2D_int::operator std::string(void) const
{
	std::ostringstream oss;
	oss<<"("<<x<<", "<<y<<")";
	return oss.str();
}

//	class Vec2
Vec2::Vec2()
{
	x=y=0.0f;
}
Vec2::Vec2(float x, float y)
{
	this->x=x;
	this->y=y;
}
Vec2::Vec2(Point2D a, Point2D b) /// creates the vector b-a (from a to b)
{
	*this = (Vec2)b-(Vec2)a;
}
Vec2::operator Point2D() const
{
	return Point2D(x,y);
}
Vec2::operator Vec3() const
{
	return Vec3(x,y,0.0f);
}
Vec2::operator std::string(void) const
{
	std::ostringstream oss;
	oss<<"("<<x<<", "<<y<<")";
	return oss.str();
}
bool Vec2::operator == (Vec2 v) const
{
	return x==v.x && y==v.y;
}
Vec3 Vec2::operator ^ (Vec2 v) const
{
	return Vec3(0.0f,0.0f,x*v.y-v.x*y);
}
float Vec2::operator *(Vec2 v) const
{
    return (x*v.x+y*v.y);
}
Vec2 Vec2::operator + (Vec2 v) const
{
	return Vec2(x+v.x, y+v.y);
}
Vec2 Vec2::operator - (Vec2 v) const
{
	return Vec2(x-v.x, y-v.y);
}
Vec2 Vec2::operator/(float k) const
{
    Vec2 tmp = *this;
    tmp.x/=k;
    tmp.y/=k;
    return tmp;
}
Vec2 Vec2::operator/=(float k)
{
    this->x/=k;
    this->y/=k;
    return *this;
}
float Vec2::getAngle(Vec2 v) const /// returns the angle between the vectors expressed in radians
{
	float sin,cos;
	sin = getSin(v);
	cos = getCos(v);
	if (sin>0)
	{
		return acos(cos);
	} else
	{
		if (cos>=0)
		{
			return 2.0f*PI_F+asin(sin);
		} else
		{
			return asin(-sin)+PI_F;
		}
	}
}
float Vec2::getSin(Vec2 v) const
{
	v.normalize();
	Vec2 v1 = getNormalized();
    return v1.x*v.y-v1.y*v.x;
}
float Vec2::getCos(Vec2 v) const
{
	return getNormalized()*v.getNormalized();
}
float Vec2::getModule(void) const
{
	return std::sqrt(x*x+y*y);
}
void Vec2::normalize(void)
{
    float l = this->getModule();
    if ( l != 0 )
    {
        *this /= l;
    }
}
Vec2 Vec2::getNormalized(void) const
{
    Vec2 tmp = *this;
    tmp.normalize();
    return tmp;
}
	
//	class Vec3
Vec3::Vec3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}
Vec3::operator std::string(void) const
{
	std::ostringstream oss;
	oss<<"("<<x<<", "<<y<<", "<<z<<")";
	return oss.str();
}

// class Edge2D
Edge2D::operator std::string(void) const
{
	std::ostringstream oss;
	oss<<"[ "<<(std::string)vertices[0]<<"_"<<(std::string)vertices[1]<<" ]";
	return oss.str();
}
Point2D Edge2D::getCentroid() const
{
	return ((Vec2)vertices[0]+(Vec2)vertices[1])/2.0f;
}
	
//	class Triangle2D
Triangle2D::Triangle2D()
{

}
Triangle2D::Triangle2D(Point2D a, Point2D b, Point2D c, NormalDir nd) ///creates a triangle with the specified normal
{
	this->a = a;
	this->b = b;
	this->c = c;
	
	calcOrientation();
	changeOrientation(nd);
}
Triangle2D::Triangle2D(Point2D a, Point2D b, Point2D c) ///creates a triangle as specified
{
	this->a = a;
	this->b = b;
	this->c = c;
	calcOrientation();
}
Triangle2D::operator std::string(void)
{
	std::ostringstream oss;
	oss<<"[ "<<(std::string)a<<"_"<<(std::string)b<<"_"<<(std::string)c<<" ]";
	return oss.str();
}
void Triangle2D::calcOrientation(void)
{
	nd = Point2D::getNormalDir(a,b,c);
}
NormalDir Triangle2D::getNormalDir()
{
	return Point2D::getNormalDir(a,b,c);
}
void Triangle2D::changeOrientation(NormalDir nd)
{
	if (this->nd == nd)
	{
		return;
	}
	Point2D tmp = b;
	b = c;
	c = tmp;
	this->nd = nd;
}
bool Triangle2D::isPointInside(Point2D p)
{
	return whereIsPoint(p)==INSIDE;
}
bool Triangle2D::isPointOutside(Point2D p)
{
	return whereIsPoint(p)==OUTSIDE;
}
bool Triangle2D::isPointOnBorder(Point2D p)
{
	return whereIsPoint(p)==ONBORDER;
}
PRP Triangle2D::whereIsPoint(Point2D p)
{
	NormalDir ab,bc,ca;
	ab = Point2D::getNormalDir(a,b,p);
	bc = Point2D::getNormalDir(b,c,p);
	ca = Point2D::getNormalDir(c,a,p);
	NormalDir up = UP, down = DOWN;
	if (nd==DOWN)
	{
		up = DOWN;
		down = UP;
	}
	if (ab==up)
	{
		if (bc==up)
		{
			if (ca==up)
			{
				return INSIDE;
			} else
			if (ca==down)
			{
				return OUTSIDE;
			} else
			{
				return ONBORDER; // on CA
			}
		} else
		if (bc==down)
		{
			return OUTSIDE;
		} else
		{
			if (ca==up)
			{
				return ONBORDER; // on BC
			} else
			if (ca==down)
			{
				return OUTSIDE;
			} else
			{
				return ONBORDER; // on C
			}
		}
	} else
	if (ab==down)
	{
		return OUTSIDE;
	} else
	{
		if (bc==NONE)
		{
			return ONBORDER; // on B
		} else
		if (bc==down)
		{
			return OUTSIDE;
		} else
		{
			if (ca==NONE)
			{
				return ONBORDER; // on A
			} else
			if (ca==up)
			{
				return ONBORDER; // on AB
			} else
			{
				return OUTSIDE;
			}
		}
	}
}
Vec2 Triangle2D::getCentroid(void) const
{
	return ((Vec2)a+(Vec2)b+(Vec2)c)/3.0f;
}
Point2D Triangle2D::getA()
{
	return a;
}
Point2D Triangle2D::getB()
{
	return b;
}
Point2D Triangle2D::getC()
{
	return c;
}
		
Point2D Triangle2D::setA(Point2D a)
{
	this->a = a;
	calcOrientation();
	return this->a;
}
Point2D Triangle2D::setB(Point2D b)
{
	this->b = b;
	calcOrientation();
	return this->b;
}
Point2D Triangle2D::setC(Point2D c)
{
	this->c = c;
	calcOrientation();
	return this->c;
}

//	class Polygon2D
Polygon2D::Polygon2D()
{
	nd = NONE;
	size = 0;
	max_size = 0;
	fixed_size = false;
	vertices_a = NULL;
}
Polygon2D::Polygon2D(int size)
{
	nd = NONE;
	this->size = 0;
	max_size = size;
	fixed_size = true;
	vertices_a = new Point2D[max_size];
}
Polygon2D::~Polygon2D(void)
{
	if (vertices_a!=NULL)
	{
		delete [] vertices_a;
	}
}
void Polygon2D::calcOrientation()
{
	float angle = 0;
	float barrier = PI_F*(float)(vertices.getCount()-2);
	DoubleLinkedCircularList<Point2D>::iterator vit = vertices.head();
	bool looped = false;
	while (!looped)
	{
		Point2D a,b,c;
		a = *vit;
		b = *(vit+1);
		c = *(vit+2);
		angle += Point2D::getAngle(a,b,c);
		++vit;
		if (vit==vertices.head())
		{
			looped = true;
		}
	}
	nd = equalWithinEpsilon(angle,barrier,2*SIN_EPS)?UP:DOWN;
}
Polygon2D::operator std::string(void)
{
	std::ostringstream oss;
	DoubleLinkedCircularList<Point2D>::iterator vit;
	oss<<"[ ";
	int loops = 0;
	for (vit = vertices.head() ; loops==0 ; )
	{
		oss<<(std::string)*vit<<"_";
		vit++;
		if (vit==vertices.head())
		{
			loops++;
		}
	}
	oss.seekp(oss.tellp()-1);
	oss<<" ]";
	return oss.str();
}
bool Polygon2D::isSizeFixed(void)
{
	return fixed_size;
}
void Polygon2D::fixSize(void)
{
	if (fixed_size)
	{
		return;
	}
	vertices_a = new Point2D[size];
	max_size = size;
	int i = 0;
	for (DoubleLinkedCircularList<Point2D>::iterator it = vertices.head() ; vertices.getCount()!=0 ; it.deleteAndNext())
	{
		vertices_a[i] = *it;
		i++;
	}
	fixed_size = true;
}
int Polygon2D::getSize(void)
{
	if (fixed_size)
	{
		return size;
	}
	return vertices.getCount();
}
NormalDir Polygon2D::getNormalDir()
{
	if (nd==NONE)
	{
		calcOrientation();
	}
	return nd;
}
void Polygon2D::changeOrientation(NormalDir nd)
{
	if (nd==NONE)
	{
		calcOrientation();
	}
	if (this->nd == nd)
	{
		return;
	}
	if (fixed_size)
	{
		Point2D tmp;
		for (int i = 0 ; i<size/2 ; i++)
		{
			tmp = vertices_a[i];
			vertices_a[i] = vertices_a[size-i-1];
			vertices_a[size-i-1] = tmp;
		}
	} else
	{
		vertices.reverse();
	}
	this->nd = nd;
}
void Polygon2D::addVertex(Point2D p)
{
	if ((vertices.getCount()!=0 && *(vertices.head()+1)==p))
	{
		return;
	}
	if (fixed_size)
	{
		if (size<max_size)
		{
			vertices_a[size] = p;
		} else
		{
			return;
		}
	} else
	{
		vertices.head().insertPrev(p);
	}
	size++;
}
ITrMesh2D& Polygon2D::triangulate(ITrMesh2D &trm)
{
	fixSize();
	Point2D v1,v2,v3;
//	DoubleLinkedCircularList<Point2D> vs (vertices);
	DoubleLinkedCircularList<PT_aux> vs ;
	DoubleLinkedCircularList<PT_aux>::iterator vit = vs.head();
	ITrMesh2D_Triangle* TT_a[3];
	for (int i=0 ; i<size ; i++)
	{
		vit.insertNext(PT_aux(i,NULL));
		vit++;
	}
	vit++;
	v1 = vertices_a[(*vit).p];

	while(vs.getCount()>3)
	{
#ifdef DEBUG_TRIANGULATEPOLYGON
		std::cout<<"WHILE LOOP..."<<(std::string)vertices_a[(*vit).p]<<std::endl;
#endif
		
		v2 = vertices_a[(*(vit+1)).p];
		v3 = vertices_a[(*(vit+2)).p];
#ifdef DEBUG_TRIANGULATEPOLYGON
		std::cout<<"testing: "<<(std::string)Triangle2D(v1,v2,v3)<<"..."<<std::endl;
#endif
		if (Point2D::getNormalDir(v1,v2,v3)==UP)
		{
			Triangle2D t (v1,v2,v3);
#ifdef DEBUG_TRIANGULATEPOLYGON
			std::cout<<"  tt:"<<(std::string)t<<std::endl;
#endif
			DoubleLinkedCircularList<PT_aux>::iterator vit2 = vit+3;
			bool can_trim = true;
			while(vit2!=vit)
			{
#ifdef DEBUG_TRIANGULATEPOLYGON
				std::cout<<"	WHILE LOOP... "<<(std::string)vertices_a[(*vit2).p]<<std::endl;
#endif
				if (!t.isPointOutside(vertices_a[(*vit2).p]))
				{
#ifdef DEBUG_TRIANGULATEPOLYGON
					std::cout<<"found a point inside the trinangle!"<<std::endl;
#endif
					v1 = v2;
					++vit;
					can_trim = false;
					break;
				}
				++vit2;
			}
#ifdef DEBUG_TRIANGULATEPOLYGON
			std::cout<<"	endWHILE LOOP "<<(std::string)vertices_a[(*vit2).p]<<std::endl;
#endif
			if (can_trim)
			{
				TT_a[0] = (*vit).t;
				TT_a[1] = (*(vit+1)).t;
				TT_a[2] = NULL;
				ITrMesh2D_Triangle& tr = trm.addTriangle(t, TT_a);
				vit.deleteNext();
				(*vit).t = &tr;
				vit--;
				v1 = vertices_a[(*vit).p];
			}
		} else
		{
			++vit;
			v1 = vertices_a[(*vit).p];
		}
	}
	Point2D p2 = vertices_a[(*(vit+1)).p];
	Point2D p3 = vertices_a[(*(vit+2)).p];
	TT_a[0] = (*vit).t;
	TT_a[1] = (*(vit+1)).t;
	TT_a[2] = (*(vit+2)).t;
	trm.addTriangle(Triangle2D(v1,p2,p3), TT_a);
//	trm.addTriangle(Triangle2D(v1,*++vit,*++vit)); /// si prende i valori dopo aver calcolato tutto => sbagliato
	return trm;
}

//class TrMesh2D_DumbPrint : public ITrMesh
TrMesh2D_Vertex_TrBased TrMesh2D_DumbPrint::v = TrMesh2D_Vertex_TrBased();
TrMesh2D_Triangle_TrBased TrMesh2D_DumbPrint::t = TrMesh2D_Triangle_TrBased();
TrMesh2D_DumbPrint::TrMesh2D_DumbPrint(void)
{

}
ITrMesh2D_Vertex& TrMesh2D_DumbPrint::addPoint(Point2D p)
{
	std::cout<<"addPoint("<<(std::string)p<<")"<<std::endl;
	return TrMesh2D_DumbPrint::v;
}
ITrMesh2D_Triangle& TrMesh2D_DumbPrint::addTriangle(Triangle2D t)
{
	std::cout<<"addTriangle("<<(std::string)t<<")"<<std::endl;
	return TrMesh2D_DumbPrint::t;
}
ITrMesh2D_Triangle& TrMesh2D_DumbPrint::addTriangle(Triangle2D t, ITrMesh2D_Triangle** TT)
{
	std::cout<<"addTriangle("<<(std::string)t<<", TT:{"<<
	(TT[0]!=NULL?(std::string)(TT[0]->getTriangle()):" - ")<<","<<
	(TT[1]!=NULL?(std::string)(TT[1]->getTriangle()):" - ")<<","<<
	(TT[2]!=NULL?(std::string)(TT[2]->getTriangle()):" - ")<<"})"<<std::endl;
	return TrMesh2D_DumbPrint::t;
}
QRes TrMesh2D_DumbPrint::removePoint(Point2D p)
{
	std::cout<<"removePoint("<<(std::string)p<<")"<<std::endl;
	return NOT_THERE;
}
bool TrMesh2D_DumbPrint::isTherePoint(Point2D p) const
{
	std::cout<<"isTherePoint("<<(std::string)p<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
	return false;
}
ITrMesh2D_Vertex& TrMesh2D_DumbPrint::getVertex(Point2D p) const
{
	std::cout<<"getVertex("<<(std::string)p<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
	return TrMesh2D_DumbPrint::v;
}
ITrMesh2D_Edge& TrMesh2D_DumbPrint::getEdge(Edge2D e) const
{
	std::cout<<"getEdge("<<(std::string)e<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
ITrMesh2D_Triangle& TrMesh2D_DumbPrint::getTriangle(Triangle2D t) const
{
	std::cout<<"getTriangle("<<(std::string)t<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
	return TrMesh2D_DumbPrint::t;
}
std::list<ITrMesh2D_Vertex*> TrMesh2D_DumbPrint::getVertices() const
{
	std::cout<<"getVertices(): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex*> TrMesh2D_DumbPrint::getVertices(const ITrMesh2D_Vertex& v) const
{
	std::cout<<"getVertices("<<(std::string)v.getVertex()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex*> TrMesh2D_DumbPrint::getVertices(const ITrMesh2D_Edge& e) const
{
	std::cout<<"getVertices("<<(std::string)e.getEdge()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex*> TrMesh2D_DumbPrint::getVertices(const ITrMesh2D_Triangle& t) const
{
	std::cout<<"getVertices("<<(std::string)t.getTriangle()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Edge*> TrMesh2D_DumbPrint::getEdges() const
{
	std::cout<<"getEdges(): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Edge*> TrMesh2D_DumbPrint::getEdges(const ITrMesh2D_Vertex& v) const
{
	std::cout<<"getEdges("<<(std::string)v.getVertex()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Edge*> TrMesh2D_DumbPrint::getEdges(const ITrMesh2D_Edge& e) const
{
	std::cout<<"getEdges("<<(std::string)e.getEdge()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Edge*> TrMesh2D_DumbPrint::getEdges(const ITrMesh2D_Triangle& t) const
{
	std::cout<<"getEdges("<<(std::string)t.getTriangle()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Triangle*> TrMesh2D_DumbPrint::getTriangles() const
{
	std::cout<<"getTriangles(): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Triangle*> TrMesh2D_DumbPrint::getTriangles(const ITrMesh2D_Vertex& v) const
{
	std::cout<<"getTriangles("<<(std::string)v.getVertex()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Triangle*> TrMesh2D_DumbPrint::getTriangles(const ITrMesh2D_Edge& e) const
{
	std::cout<<"getTriangles("<<(std::string)e.getEdge()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Triangle*> TrMesh2D_DumbPrint::getTriangles(const ITrMesh2D_Triangle& t) const
{
	std::cout<<"getTriangles("<<(std::string)t.getTriangle()<<"): ?(TrMesh2D_DumbPrint do not stores any data)"<<std::endl;
}
Vec2 TrMesh2D_DumbPrint::getBottomLeftBoundary(void) const
{

}
Vec2 TrMesh2D_DumbPrint::getTopRightBoundary(void) const
{

}

//	class TrMesh2D_TrBased : public ITrMesh2D
TrMesh2D_TrBased::TrMesh2D_TrBased(int nvert, ITrMesh2D_TrBased_NewEntriesManager* new_entries_manager)
{
	if (nvert < 1)
	{
		nvert = DEF_VERT_NUM;
	} else
	if (nvert <3)
	{
		nvert = 3;
	}
	vertices = new TrMesh2D_TrBased_internalVertex[nvert];
	triangles = new TrMesh2D_TrBased_internalTriangle[nvert-2];
	vertices_maxcount = nvert;
	triangles_maxcount = nvert-2;
	vertices_count = 0;
	triangles_count = 0;
	this->new_entries_manager = new_entries_manager;
}
TrMesh2D_TrBased::~TrMesh2D_TrBased()
{
	delete [] vertices;
	delete [] triangles;
}
int TrMesh2D_TrBased::getNewPointID(void) const
{
	return vertices_count;
}
int TrMesh2D_TrBased::findVertex(Point2D p) const
{
	if (new_entries_manager==NULL)
	{
		for (int i=0 ; i<vertices_count ; i++)
		{
			if (vertices[i].vertex==p)
			{
				return i;
			}
		}
		return -1;
	} else
	{
		int nid = getNewPointID();
		int res = new_entries_manager->addingVertex(p,nid);
		return res==nid?-1:res;
	}
}
int TrMesh2D_TrBased::insertVertex(Point2D p)
{
	int i = findVertex(p);
	if ( i==-1 && vertices_count<vertices_maxcount)
	{
		i = getNewPointID();
		vertices[i] = TrMesh2D_TrBased_internalVertex(p);
		vertices_count++;
	}
	return i;
}
int TrMesh2D_TrBased::insertTriangle(Triangle2D t) ///inserts a triangle without calculating TT relation and returns his id
{
	t.changeOrientation(UP);
	int vsi[3];
	vsi[0] = insertVertex(t.getA());
	vsi[1] = insertVertex(t.getB());
	vsi[2] = insertVertex(t.getC());
	// TODO set the VT_star for those vertices with it = -1
	if (triangles_count<triangles_maxcount)
	{
		int tr_id = triangles_count;
		triangles[tr_id] = TrMesh2D_TrBased_internalTriangle(vsi); /// qui non fa il controllo dell'esistenza del triangolo
#ifdef DEBUG
		std::cout<<"inserted triangle "<<(std::string)t<<" with id "<<tr_id<<" vertices ids:"<<vsi[0]<<" "<<vsi[1]<<" "<<vsi[2]<<std::endl;
#endif
		triangles_count++;
		return tr_id;
	} else
	{
		return -1;
	}
}
ITrMesh2D_Vertex& TrMesh2D_TrBased::addPoint(Point2D p)
{
	return *(new TrMesh2D_Vertex_TrBased(p,insertVertex(p)));
}
ITrMesh2D_Triangle& TrMesh2D_TrBased::addTriangle(Triangle2D t)
{
	int tr_id = insertTriangle(t);
	if (tr_id==-1)
	{
		std::cerr	<<"Error inserting triangle!"<<std::endl;
		TrMesh2D_Triangle_TrBased* tb = new TrMesh2D_Triangle_TrBased(t, -1); /// to signal the error the id is set to -1
		return *tb;
	}
#ifdef DEBUG
	std::cout<<"TT construction..."<<std::endl;
#endif
	/**
	 * TT construction
	 **/
	int* vsi = triangles[tr_id].vertices;
	for (int i=0 ; i<3 ; i++)
	{
		ITrMesh2D_TrBased_NewEntriesManager::TT tt;
		tt = new_entries_manager->addingEdge(Point2D_int(vsi[i],vsi[(i+4)%3]), tr_id, i);
		if (tt.tr_id!=tr_id)
		{
			triangles[tr_id].TT[i] = tt.tr_id;
			triangles[tt.tr_id].TT[tt.tt_id] = tr_id;
		}
	}
	TrMesh2D_Triangle_TrBased* tb = new TrMesh2D_Triangle_TrBased(t, triangles_count-1);
	return *tb;
}
ITrMesh2D_Triangle& TrMesh2D_TrBased::addTriangle(Triangle2D t, ITrMesh2D_Triangle** TT)
{
	int tr_id = insertTriangle(t);
	TrMesh2D_Triangle_TrBased* TT_TB[3];
	for (int i=0 ; i<3 ; i++)
	{
		TT_TB[i] = dynamic_cast<TrMesh2D_Triangle_TrBased*>(TT[i]);
	}
	if (tr_id!=-1)
	{
		int ii,jj;
		jj = tr_id;
		for (int kt=0 ; kt<3 ; kt++)
		{
			if (TT_TB[kt]!=NULL && TT_TB[kt]->getIndex()>-1 && TT_TB[kt]->getIndex()<triangles_count)
			{
				int i,j;
				ii = TT_TB[kt]->getIndex();
				for (i=0 ; i<3 ; i++)
				{
					for (j=0 ; j<3 ; j++)
					{
						if (triangles[ii].vertices[i] == triangles[jj].vertices[j])
						{
							if (triangles[ii].vertices[(i+2)%3] == triangles[jj].vertices[(j+1)%3])
							{
								triangles[ii].TT[(i+2)%3] = jj;
								triangles[jj].TT[j] = ii;
								i = j = 4;
							} else
							{
								std::cerr<<"Wrong suggestion of TT_TB. Stopped at line "<<__LINE__<<" of "<<__FILE__<<std::endl;
								/*int *errorp = NULL;
								*errorp = 0;*/
								throw false;
							}
						}
					}
				}
				if (i==3)
				{
					std::cerr<<"Wrong suggestion of TT_TB. Stopped at line "<<__LINE__<<" of "<<__FILE__<<std::endl;
					/*int *errorp = NULL;
					*errorp = 0;*/
					throw false;
				}
			}
		}
	}
	return *(new TrMesh2D_Triangle_TrBased(t, tr_id));
}
QRes TrMesh2D_TrBased::removePoint(Point2D p)
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	return NOT_THERE;
}
bool TrMesh2D_TrBased::isTherePoint(Point2D p) const
{
	return findVertex(p)!=-1;
}
ITrMesh2D_Vertex& TrMesh2D_TrBased::getVertex(Point2D p) const
{
	return *(new TrMesh2D_Vertex_TrBased(p, findVertex(p)));
}
ITrMesh2D_Edge& TrMesh2D_TrBased::getEdge(Edge2D e) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
ITrMesh2D_Triangle& TrMesh2D_TrBased::getTriangle(Triangle2D t) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Vertex*> TrMesh2D_TrBased::getVertices() const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Vertex*> TrMesh2D_TrBased::getVertices(const ITrMesh2D_Vertex& v) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Vertex*> TrMesh2D_TrBased::getVertices(const ITrMesh2D_Edge& e) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Vertex*> TrMesh2D_TrBased::getVertices(const ITrMesh2D_Triangle& t) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Edge*> TrMesh2D_TrBased::getEdges() const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Edge*> TrMesh2D_TrBased::getEdges(const ITrMesh2D_Vertex& v) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Edge*> TrMesh2D_TrBased::getEdges(const ITrMesh2D_Edge& e) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Edge*> TrMesh2D_TrBased::getEdges(const ITrMesh2D_Triangle& t) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);
}
std::list<ITrMesh2D_Triangle*> TrMesh2D_TrBased::getTriangles() const
{
	std::list<ITrMesh2D_Triangle*>* res = new std::list<ITrMesh2D_Triangle*>();
	for (int i=0 ; i<triangles_count ; i++)
	{
		res->push_back(new TrMesh2D_Triangle_TrBased(Triangle2D(vertices[triangles[i].vertices[0]].vertex, 
																vertices[triangles[i].vertices[1]].vertex, 
																vertices[triangles[i].vertices[2]].vertex ), i));
	}
	return *res;
}
std::list<ITrMesh2D_Triangle*> TrMesh2D_TrBased::getTriangles(const ITrMesh2D_Vertex& v) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);
	
}
std::list<ITrMesh2D_Triangle*> TrMesh2D_TrBased::getTriangles(const ITrMesh2D_Edge& e) const
{
	std::cerr<<"Not yet implemented. "<<__LINE__<<" "<<__FILE__<<std::endl;
	exit(1);

}
std::list<ITrMesh2D_Triangle*> TrMesh2D_TrBased::getTriangles(const ITrMesh2D_Triangle& t) const
{
	std::list<ITrMesh2D_Triangle*>* res = new std::list<ITrMesh2D_Triangle*>();
	TrMesh2D_Triangle_TrBased& tt = dynamic_cast<TrMesh2D_Triangle_TrBased&>(const_cast<ITrMesh2D_Triangle&>(t));
	for (int i=0 ; i<3 ; i++)
	{
		if (triangles[tt.getIndex()].TT[i]!=-1)
		{
			res->push_back(new TrMesh2D_Triangle_TrBased(Triangle2D(vertices[triangles[triangles[tt.getIndex()].TT[i]].vertices[0]].vertex, 
																	vertices[triangles[triangles[tt.getIndex()].TT[i]].vertices[1]].vertex, 
																	vertices[triangles[triangles[tt.getIndex()].TT[i]].vertices[2]].vertex ), triangles[tt.getIndex()].TT[i]));
		}
	}
	return *res;
}
Vec2 TrMesh2D_TrBased::getBottomLeftBoundary(void) const
{
	Vec2 bl(FLT_MAX,FLT_MAX);
	for (int i=0 ; i<vertices_count ; i++)
	{
		if (vertices[i].vertex.x<bl.x)
		{
			bl.x = vertices[i].vertex.x;
		}
		if (vertices[i].vertex.y<bl.y)
		{
			bl.y = vertices[i].vertex.y;
		}
	}
	return bl;
}
Vec2 TrMesh2D_TrBased::getTopRightBoundary(void) const
{
	Vec2 tr(FLT_MIN, FLT_MIN);
	for (int i=0 ; i<vertices_count ; i++)
	{
		if (vertices[i].vertex.x>tr.x)
		{
			tr.x = vertices[i].vertex.x;
		}
		if (vertices[i].vertex.y>tr.y)
		{
			tr.y = vertices[i].vertex.y;
		}
	}
	return tr;
}
void TrMesh2D_TrBased::setExpectedBottomLeftBoundary(Vec2 bl)
{
	bottom_left_expected = bl;
}
void TrMesh2D_TrBased::setExpectedTopRightBoundary(Vec2 tr)
{
	top_right_expected = tr;
}
Vec2 TrMesh2D_TrBased::getExpectedBottomLeftBoundary(void) const
{
	return bottom_left_expected;
}
Vec2 TrMesh2D_TrBased::getExpectedTopRightBoundary(void) const
{
	return top_right_expected;
}
Point2D TrMesh2D_TrBased::getVertex(int id)
{
	return vertices[id].vertex;
}
int TrMesh2D_TrBased::getVerticesCount(void)
{
	return vertices_count;
}
int TrMesh2D_TrBased::getTrianglesCount(void)
{
	return triangles_count;
}
int TrMesh2D_TrBased::getVerticesMaxCount(void)
{
	return vertices_maxcount;
}
int TrMesh2D_TrBased::getTrianglesMaxCount(void)
{
	return triangles_maxcount;
}

//	class TrMesh2D_TrBased_internalVertex
TrMesh2D_TrBased_internalVertex::TrMesh2D_TrBased_internalVertex(Point2D vertex, int VT_star)
{
	this->vertex = vertex;
	this->VT_star = VT_star;
}

//	class TrMesh2D_TrBased_internalTriangle
TrMesh2D_TrBased_internalTriangle::TrMesh2D_TrBased_internalTriangle()
{
	TT[0] = TT[1] = TT[2] = vertices[0] = vertices[1] = vertices[2] = -1;
}
TrMesh2D_TrBased_internalTriangle::TrMesh2D_TrBased_internalTriangle(int vertices[3])
{
	memcpy(this->vertices, &vertices[0], sizeof(int)*3);
	TT[0] = TT[1] = TT[2] = -1;
}

//	class TrMesh2D_Vertex_TrBased
TrMesh2D_Vertex_TrBased::TrMesh2D_Vertex_TrBased(Point2D vertex, int index)
{
	this->vertex = vertex;
	this->index = index;
}
Point2D TrMesh2D_Vertex_TrBased::getVertex() const
{
	return vertex;
}
int TrMesh2D_Vertex_TrBased::getIndex(void) const
{
	return index;
}

//	class TrMesh2D_Triangle_TrBased
TrMesh2D_Triangle_TrBased::TrMesh2D_Triangle_TrBased(Triangle2D t, int index)
{
	this->t = t;
	this->index = index;
}
Triangle2D TrMesh2D_Triangle_TrBased::getTriangle(void) const
{
	return t;
}
int TrMesh2D_Triangle_TrBased::getIndex(void) const
{
	return index;
}

//	class TrMesh2D_TrBased_GridNewEntriesManager
TrMesh2D_TrBased_GridNewEntriesManager::TrMesh2D_TrBased_GridNewEntriesManager(TrMesh2D_TrBased* owner, int rows, int cols, int erows):owner(owner), rows(rows), cols(cols), erows(erows)
{
	if (this->rows<0)
	{
		this->rows = 1;
	}
	if (this->cols<0)
	{
		this->cols = 1;
	}
	setOwner(owner);
}
TrMesh2D_TrBased_GridNewEntriesManager::~TrMesh2D_TrBased_GridNewEntriesManager()
{
	delete [] vertices_grid[0];
	free(vertices_grid);
	delete [] edges_pseudo_grid[0];
	free(edges_pseudo_grid);
}
int TrMesh2D_TrBased_GridNewEntriesManager::getRow(const Point2D& p) /// returns the id of the row of the point in the grid, -1 if outside
{
	int res = (p.y-origin.y)/cell_height;
#ifdef DEBUG
	std::cout<<"row for "<<(std::string)p<<" "/*<<res<<"...  "*/<<(res<0? -1 : res>=rows? (p.y==top_right.y?res-1:-1) : res)<<std::endl;
#endif
	return res<0? -1 : res>=rows? (p.y==top_right.y?res-1:-1) : res;
}
int TrMesh2D_TrBased_GridNewEntriesManager::getCol(const Point2D& p) /// returns the id of the col of the point in the grid, -1 if outside
{
	int res = (p.x-origin.x)/cell_width;
	return res<0? -1 : res>=cols? (p.x==top_right.x?res-1:-1) : res;
}
int TrMesh2D_TrBased_GridNewEntriesManager::getECoord(int id)
{
	int res = id/ecells_edge;
	if (res<0||res>=erows)
	{
		res = -1;
	}
	return res;
}
void TrMesh2D_TrBased_GridNewEntriesManager::setOwner(TrMesh2D_TrBased* owner)
{
	this->owner = owner;
	if (owner!=NULL)
	{
#ifdef DEBUG
		std::cout<<rows<<" "<<cols<<" "<<erows<<" "<<owner<<std::endl;
#endif
		vertices_grid = (std::list<int>**)malloc(rows*sizeof(std::list<int>*));
		std::list<int>* data = new std::list<int>[rows*cols];//(std::list<int>*)malloc(rows*cols*sizeof(std::list<int>));
		for (int i=0 ; i<rows ; i++, data+=cols)
		{
			vertices_grid[i] = data;
			for (int c=0 ; c<cols ; c++)
			{
//				vertices_grid[i][c]();
			}
		}
		edges_pseudo_grid = (std::list<AuxEdge>**)malloc(erows*sizeof(std::list<AuxEdge>*));
		std::list<AuxEdge>* data_e = new std::list<AuxEdge>[(erows*(erows+1))/2];//(std::list<AuxEdge>*)malloc(((erows*(erows+1))/2)*sizeof(std::list<AuxEdge>));
		for (int i=0 ; i<erows ; data_e+=++i)
		{
			edges_pseudo_grid[i] = data_e;
			for (int c=0 ; c<=i ; c++)
			{
//				edges_pseudo_grid[i][c]();
			}
		}
		ecells_edge = std::ceil(owner->getVerticesMaxCount()/(float)erows);
#ifdef DEBUG
		std::cout<<"ecells_edge:"<<ecells_edge<<std::endl;
#endif
		origin = owner->getExpectedBottomLeftBoundary();
		top_right = owner->getExpectedTopRightBoundary();
		cell_height = (top_right.y-origin.y)/(float)rows;
		cell_width = (top_right.x-origin.x)/(float)cols;
#ifdef DEBUG
		std::cout<<"cell_h:"<<cell_height<<"  cell_w:"<<cell_width;
#endif
	}
}
int TrMesh2D_TrBased_GridNewEntriesManager::addingVertex(Point2D p, int new_id) /// returns the id of the vertex previously added or adds the new vertex and returns new_id (that should be the id of the point if it would be added)
{
	int col, row;
	col = getCol(p);
	row = getRow(p);
#ifdef DEBUG
	std::cout<<"adding vertex new_id: "<<new_id<<std::endl;
#endif
	if (col==-1 || row==-1)
	{
#ifdef DEBUG
		std::cout<<"You should not try to insert a vertex out from your expected bounds!"<<std::endl;
#endif
		return -1;
	}
	
	for (std::list<int>::iterator it = vertices_grid[row][col].begin() ; it!=vertices_grid[row][col].end() ; it++)
	{
#ifdef DEBUG
		std::cout<<"examinating vertex "<<*it<<std::endl;
#endif
		if (owner->getVertex(*it)==p)
		{
#ifdef DEBUG
			std::cout<<"vertex found!"<<std::endl;
#endif
			return *it;
		}
	}
	vertices_grid[row][col].push_back(new_id);
	return new_id;
}
ITrMesh2D_TrBased_NewEntriesManager::TT TrMesh2D_TrBased_GridNewEntriesManager::addingEdge(Point2D_int e, int tr_id, int tt_id) /// returns a TT with the id of the triangle that owns the edge and the relative id of TT if previously added, tr_id and tt_id if it's new or -1 if error. tr_id is the id of the owner triangle of e. tt is the id of the TT relation based on e for tr_id
{
	int col, row;
	if (e.x>e.y) // condition of the pseudogrid is col<=row
	{
		/**
		 *	we have to switch e ids	
		 **/
		int tmp = e.x;
		e.x=e.y;
		e.y=tmp;
	}
	col = getECoord(e.x);
	row = getECoord(e.y);
	if (col==-1 || row==-1)
	{
		return -1;
	}
#ifdef DEBUG
	std::cout<<"searching for edge "<<(std::string)e<<"...";
#endif
	for (std::list<AuxEdge>::iterator it = edges_pseudo_grid[row][col].begin() ; it!=edges_pseudo_grid[row][col].end() ; it++)
	{
		if ((*it).e==e)
		{
#ifdef DEBUG
			std::cout<<"found!"<<std::endl;
#endif
			TT res = TT((*it).tr_id,(*it).tt_id);
			edges_pseudo_grid[row][col].erase(it);
			return res;
		}
	}
#ifdef DEBUG
	std::cout<<"not found"<<std::endl;
#endif
	edges_pseudo_grid[row][col].push_back(AuxEdge(e,tr_id,tt_id));
	return TT(tr_id,tt_id);
}

//	class class TrMesh2D_TrBased_PRQTreeNewEntriesManager
TrMesh2D_TrBased_PRQTreeNewEntriesManager::TrMesh2D_TrBased_PRQTreeNewEntriesManager(TrMesh2D_TrBased* owner) : owner(owner)
{
	root_points = root_edges = NULL;
	setOwner(owner);
}
int TrMesh2D_TrBased_PRQTreeNewEntriesManager::getChildIdNode(Point2D& bl, Point2D& tr, Point2D& p)
{
	int child;
	if (p.x<bl.x)					/// out west
	{
		if (p.y<bl.y)
		{
			child = OutSouthWest;		/// out southwest 
		} else
		if (p.y>=tr.y)
		{
			child = OutNorthWest;		/// out northwest
		} else
		{
			child = OutWest;			/// out west
		}
	} else
	if (p.x>=tr.x)					/// out east
	{
		if (p.y<bl.y)
		{
			child = OutSouthEast;		/// out southeast
		} else
		if (p.y>=tr.y)
		{
			child = OutNorthEast;		/// out notrheast
		} else
		{
			child = OutEast;			///out east
		}
	} else							/// inside
	{
		Point2D center = ((Vec2)bl+(Vec2)tr)/2.0f;
		if (p.x<center.x)				/// west
		{
			if (p.y<center.y)
			{
				child = SouthWest;			/// southwest
			} else
			{
				child = NorthWest;			/// northwest
			}
		} else							/// east
		{
			if (p.y<center.y)
			{
				child = SouthEast;			/// southeast
			} else
			{
				child = NorthEast;			///northeast
			}
		}
	}
	return child;
}
int TrMesh2D_TrBased_PRQTreeNewEntriesManager::getChildIdNode(Point2D_int& bl, Point2D_int& tr, Point2D_int& p)
{
	int child;
	Point2D_int center((bl.x+tr.x)/2,(bl.y+tr.y)/2);
	if (p.x<center.x)				/// west
	{
		if (p.y<center.y)
		{
			child = SouthWest;			/// southwest
		} else
		{
			child = NorthWest;			/// northwest
		}
	} else							/// east
	{
		if (p.y<center.y)
		{
			child = SouthEast;			/// southeast
		} else
		{
			child = NorthEast;			///northeast
		}
	}
	return child;
}
int TrMesh2D_TrBased_PRQTreeNewEntriesManager::calcChildBoundaries(int* child, Point2D* bl, Point2D* tr)
{
	Point2D center = ((Vec2)*bl+(Vec2)*tr)/2.0f;
	int res = *child;
	switch (*child)
	{
		case NorthWest:
			bl->y = center.y;
			tr->x = center.x;
			break;
		case NorthEast:
			*bl = center;
			break;
		case SouthWest:
			*tr = center;
			break;
		case SouthEast:
			bl->x = center.x;
			tr->y = center.y;
			break;
		case OutNorthWest:
			res = SouthEast;
			*child = NorthWest;
			bl->x-=tr->x-bl->x;
			tr->y +=tr->y-bl->y;
			break;
		case OutNorth:
			res = SouthEast;
			*child = NorthEast;
			bl->x-=tr->x-bl->x;
			tr->y +=tr->y-bl->y;
			break;
		case OutNorthEast:
			res = SouthWest;
			*child = NorthEast;
			tr->x+=tr->x-bl->x;
			tr->y+=tr->y-bl->y;
			break;
		case OutWest:
			res = NorthEast;
			*child = NorthWest;
			bl->x-=tr->x-bl->x;
			bl->y-=tr->y-bl->y;
			break;
		case OutEast:
			res = SouthWest;
			*child = SouthEast;
			tr->x+=tr->x-bl->x;
			tr->y+=tr->y-bl->y;
			break;
		case OutSouthWest:
			res = NorthEast;
			*child = SouthWest;
			bl->x-=tr->x-bl->x;
			bl->y-=tr->y-bl->y;
			break;
		case OutSouth:
			res = NorthWest;
			*child = SouthWest;
			bl->y-=tr->y-bl->y;
			tr->x+=tr->x-bl->x;
			break;
		case OutSouthEast:
			res = NorthWest;
			*child = SouthEast;
			bl->y-=tr->y-bl->y;
			tr->x+=tr->x-bl->x;
			break;
	}
	return res;
}
void TrMesh2D_TrBased_PRQTreeNewEntriesManager::calcFatherBoundaries(int child, Point2D* bl, Point2D* tr)
{
	switch(child)
	{
		case NorthWest:
			bl->y-=tr->y-bl->y;
			tr->x+=tr->x-bl->x;
			break;
		case NorthEast:
			bl->y-=tr->y-bl->y;
			bl->x-=tr->x-bl->x;
			break;
		case SouthWest:
			tr->y+=tr->y-bl->y;
			tr->x+=tr->x-bl->x;
			break;
		case SouthEast:
			bl->x-=tr->x-bl->x;
			tr->y+=tr->y-bl->y;
			break;
		default:
			std::cerr<<"Error! at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
			exit(1);
	}
}
int TrMesh2D_TrBased_PRQTreeNewEntriesManager::calcChildBoundaries(int* child, Point2D_int* bl, Point2D_int* tr)
{
	Point2D_int center((bl->x+tr->x)/2,(bl->y+tr->y)/2);
	int res = *child;
	switch (*child)
	{
		case NorthWest:
			bl->y = center.y;
			tr->x = center.x;
			break;
		case NorthEast:
			*bl = center;
			break;
		case SouthWest:
			*tr = center;
			break;
		case SouthEast:
			bl->x = center.x;
			tr->y = center.y;
			break;
	}
	return res;
}
		
int TrMesh2D_TrBased_PRQTreeNewEntriesManager::getOppositeChild(int child)
{
	return (child+2)%4;
}
void TrMesh2D_TrBased_PRQTreeNewEntriesManager::setOwner(TrMesh2D_TrBased* owner)
{
	this->owner = owner;
	if (owner!=NULL)
	{
		bl = owner->getExpectedBottomLeftBoundary();
		tr = owner->getExpectedTopRightBoundary();
		verticesmaxcount = owner->getVerticesMaxCount();
	}
}
int TrMesh2D_TrBased_PRQTreeNewEntriesManager::addingVertex(Point2D p, int new_id) /// returns the id of the vertex previously added or adds the new vertex and returns new_id (that should be the id of the point if it would be added)
{
#ifdef DEBUG
	std::cout<<"addingVertex "<<(std::string)p<<" "<<new_id<<std::endl;
#endif
	if (root_points==NULL)
	{
		if (bl==tr)
		{
			bl = (Vec2)p+Vec2(-1,-1);
			tr = (Vec2)p+Vec2(1,1);
		}
	}
	Point2D rel_bl = bl;
	Point2D rel_tr = tr;
	QTNode* qtn;
	QTInternalNode* father_qtn=NULL;
	int child;
	for (qtn = root_points ; qtn!=NULL ; father_qtn = reinterpret_cast<QTInternalNode*>(qtn), qtn = father_qtn->children[child])
	{
		if (qtn->getType()==QTNode::leaf_type)
		{
			int id = reinterpret_cast<QTLeafNode<int>*>(qtn)->val;
			Point2D leaf_val = owner->getVertex(id);
			if (leaf_val == p)
			{
				return id;
			} else
			{
				QTInternalNode* nc = new QTInternalNode();
				nc->children[getChildIdNode(rel_bl,rel_tr,leaf_val)] = qtn;
				qtn = nc;
				if (father_qtn!=NULL)
				{
					father_qtn->children[child] = qtn;
				} else
				{
					root_points = qtn;
				}
			}
		}
		int new_child = getChildIdNode(rel_bl,rel_tr,p);
		if (new_child<4)	///inside
		{
			calcChildBoundaries(&new_child,&rel_bl,&rel_tr);
			child = new_child;
		} else				/// outside
		{
			if (father_qtn!=NULL)
			{
				//entering from the root here the father of father_qtn is root_points
				qtn = father_qtn;
				father_qtn = NULL;
				int nn_child = getOppositeChild(child);
				calcFatherBoundaries(nn_child,&rel_bl,&rel_tr);
				QTInternalNode* nn = new QTInternalNode();
				nn->children[nn_child] = qtn;
				qtn = root_points = nn;
				bl = rel_bl;
				tr = rel_tr;
				new_child = getChildIdNode(rel_bl,rel_tr,p);
//				std::cerr<<"Error! at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
//				/// entering from the root this can not happen
//				exit(1);
			}
			QTInternalNode* nn = new QTInternalNode();
			nn->children[calcChildBoundaries(&new_child,&rel_bl,&rel_tr)] = qtn;
			bl = rel_bl;
			tr = rel_tr;
			qtn = root_points = nn;
			child = new_child;
		}
	}
	// qtn is NULL and =father_qtn->children[child]
	if (father_qtn==NULL)
	{
		root_points = new QTLeafNode<int>(new_id);
		return new_id;
	}
	father_qtn->children[child] = new QTLeafNode<int>(new_id);
	return new_id;
}
ITrMesh2D_TrBased_NewEntriesManager::TT TrMesh2D_TrBased_PRQTreeNewEntriesManager::addingEdge(Point2D_int p, int tr_id, int tt_id) /// returns a TT with the id of the triangle that owns the edge and the relative id of TT if previously added, tr_id and tt_id if it's new or -1 if error. tr_id is the id of the owner triangle of e. tt is the id of the TT relation based on e for tr_id
{
	/**
	 * To balance the three we divide the points in two groups with the sum of the coordinates
	 * even or odd; the first will be set to have x>=y and the second x<y
	 **/
	if ((p.x+p.y)%2)
	{
		if (p.x>=p.y)
		{
			int tmp = p.x;
			p.x = p.y;
			p.y = tmp;
		}
	} else
	{
		if (p.x<p.y)
		{
			int tmp = p.x;
			p.x = p.y;
			p.y = tmp;
		}
	}
#ifdef DEBUGPRQTREES
	std::cout<<std::endl<<"Inserting edge "<<(std::string)p<<"on:"<<std::endl;
	printEdgesTree();
#endif
	
	Point2D_int rel_bl(0,0);
	Point2D_int rel_tr(verticesmaxcount,verticesmaxcount);
	QTNode* qtn;
	QTInternalNode* father_qtn=NULL;
	int child;
	for (qtn = root_edges ; qtn!=NULL ; father_qtn = reinterpret_cast<QTInternalNode*>(qtn), qtn = father_qtn->children[child])
	{
		if (qtn->getType()==QTNode::leaf_type)
		{
			AuxEdge ae = reinterpret_cast<QTLeafNode<AuxEdge>*>(qtn)->val;
			if (ae.e == p)
			{
				if (father_qtn!=NULL)
				{
					delete father_qtn->children[child];
					father_qtn->children[child] = NULL;
				} else
				{
					delete root_edges;
				}
				return ITrMesh2D_TrBased_NewEntriesManager::TT(ae.tr_id,ae.tt_id);
			} else
			{
				QTInternalNode* nc = new QTInternalNode();
				nc->children[getChildIdNode(rel_bl,rel_tr,ae.e)] = qtn;
				qtn = nc;
				if (father_qtn!=NULL)
				{
					father_qtn->children[child] = qtn;
				} else
				{
					root_edges = qtn;
				}
			}
		}
		int new_child = getChildIdNode(rel_bl,rel_tr,p);
		if (new_child<4)	///inside
		{
			calcChildBoundaries(&new_child,&rel_bl,&rel_tr);
			child = new_child;
		}
	}
	// qtn is NULL and =father_qtn->children[child]
	if (father_qtn==NULL)
	{
#ifdef DEBUG
		if (root_edges!=NULL)
		{
			std::cerr<<"IMPOSSIBLE! on line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
			exit(1);
		}
#endif
		root_edges = new QTLeafNode<AuxEdge>(AuxEdge(p,tr_id,tt_id));
	} else
	{
#ifdef DEBUG
		if (father_qtn->children[child]!=NULL)
		{
			std::cerr<<"VERY WRONG! on line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
			exit(1);
		}
#endif
		father_qtn->children[child] = new QTLeafNode<AuxEdge>(AuxEdge(p,tr_id,tt_id));
	}
	return ITrMesh2D_TrBased_NewEntriesManager::TT(tr_id,tt_id);
}

void TrMesh2D_TrBased_PRQTreeNewEntriesManager::aux_printVerticesTree(QTNode*node, int level)
{
	for (int i=0 ; i<level ; i++)
	{
		std::cout<<"   ";
	}
	if (node==NULL)
	{
		std::cout<<"-"<<std::endl;
		return;
	} else
	{
		std::cout<<node;
	}
	if (node->getType()==QTNode::internal_type)
	{
		std::cout<<std::endl;
		for (int i=0 ; i<4 ; i++)
		{
			aux_printVerticesTree(reinterpret_cast<QTInternalNode*>(node)->children[i], level+1);
		}
	}
	else if (node->getType()==QTNode::leaf_type)
	{
		std::cout<<" leaf val="<<(reinterpret_cast<QTLeafNode<int>*>(node)->val)<<std::endl;
	} else
	{
		std::cerr<<"Node of wrong type"<<std::endl;
		exit(1);
	}
}
void TrMesh2D_TrBased_PRQTreeNewEntriesManager::aux_printEdgesTree(QTNode*node, int level)
{
	for (int i=0 ; i<level ; i++)
	{
		std::cout<<"   ";
	}
	if (node==NULL)
	{
		std::cout<<"-"<<std::endl;
		return;
	} else
	{
		std::cout<<node;
	}
	if (node->getType()==QTNode::internal_type)
	{
		std::cout<<std::endl;
		for (int i=0 ; i<4 ; i++)
		{
			aux_printEdgesTree(reinterpret_cast<QTInternalNode*>(node)->children[i], level+1);
		}
	} else
	if (node->getType()==QTNode::leaf_type)
	{
		AuxEdge ae = reinterpret_cast<QTLeafNode<AuxEdge>*>(node)->val;
		Point2D_int p = ae.e;
		std::cout<<" leaf val="<<(std::string)(p)<<std::endl;
	} else
	{
		std::cerr<<"Node of wrong type"<<std::endl;
		exit(1);
	}
}
void TrMesh2D_TrBased_PRQTreeNewEntriesManager::printVerticesTree()
{
	aux_printVerticesTree(root_points, 0);
}
void TrMesh2D_TrBased_PRQTreeNewEntriesManager::printEdgesTree()
{
	aux_printEdgesTree(root_edges,0);
}























