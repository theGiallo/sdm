#include "libGeometry.h"
#include <cmath>
#include <iostream>
#include <sstream>

using namespace libGeometry;

// namespace functions
bool libGeometry::equalWithinEpsilon(float v1, float v2, float eps)
{
	return abs(v1-v2)<=abs(eps);
}

//class Point2D
Point2D::Point2D()
{
	x=y=0.0f;
}
Point2D::Point2D(float x, float y)
{
	this->x=x;
	this->y=y;
}
Point2D::operator Vec2()
{
	return Vec2(x,y);
}
Point2D::operator std::string(void)
{
	std::ostringstream oss;
	oss<<"("<<x<<", "<<y<<")";
	return oss.str();
}
bool Point2D::operator == (Point2D p)
{
	return x==p.x && y==p.y;
}		
float Point2D::getAngle(Point2D a, Point2D b, Point2D c) /// returns the angle abcexpressed in radians
{
	return Vec2(b,c).getAngle(Vec2(b,a));
}
float Point2D::getSin(Point2D a, Point2D b, Point2D c)
{
    return Vec2(b,c).getSin(Vec2(b,a));
}
float Point2D::getCos(Point2D a, Point2D b, Point2D c)
{
	return Vec2(b,c).getCos(Vec2(b,a));
}
float Point2D::getNormalModule(Point2D a, Point2D b, Point2D c)
{
	return getSin(a, b, c);
}
NormalDir Point2D::getNormalDir(Point2D a, Point2D b, Point2D c)
{
	float s = Point2D::getSin(a,b,c);
	if (s>-SIN_EPS && s<SIN_EPS)
	{
		s=0;
		return NONE;
	}
	return s>0?UP:DOWN;
}
	
//	class Vec2
Vec2::Vec2()
{
	x=y=0.0f;
}
Vec2::Vec2(float x, float y)
{
	this->x=x;
	this->y=y;
}
Vec2::Vec2(Point2D a, Point2D b) /// creates the vector b-a (from a to b)
{
	*this = (Vec2)b-(Vec2)a;
}
Vec2::operator Point2D()
{
	return Point2D(x,y);
}
Vec2::operator Vec3()
{
	return Vec3(x,y,0.0f);
}
Vec2::operator std::string(void)
{
	std::ostringstream oss;
	oss<<"("<<x<<", "<<y<<")";
	return oss.str();
}
bool Vec2::operator == (Vec2 v)
{
	return x==v.x && y==v.y;
}
Vec3 Vec2::operator ^ (Vec2 v)
{
	return Vec3(0.0f,0.0f,x*v.y-v.x*y);
}
float Vec2::operator *(Vec2 v)
{
    return (x*v.x+y*v.y);
}
Vec2 Vec2::operator + (Vec2 v)
{
	return Vec2(x+v.x, y+v.y);
}
Vec2 Vec2::operator - (Vec2 v)
{
	return Vec2(x-v.x, y-v.y);
}		
Vec2 Vec2::operator/(float k)
{
    Vec2 tmp = *this;
    tmp.x/=k;
    tmp.y/=k;
    return tmp;
}
Vec2 Vec2::operator/=(float k)
{
    this->x/=k;
    this->y/=k;
    return *this;
}
float Vec2::getAngle(Vec2 v) /// returns the angle between the vectors expressed in radians
{
	float sin,cos;
	sin = getSin(v);
	cos = getCos(v);
	if (sin>0)
	{
		return acos(cos);
	} else
	{
		if (cos>0)
		{
			return 2.0f*PI_F+asin(sin);
		} else
		{
			return asin(sin)+PI_F;
		}
	}
}
float Vec2::getSin(Vec2 v)
{
	v.normalize();
	Vec2 v1 = getNormalized();
    return v1.x*v.y-v1.y*v.x;
}
float Vec2::getCos(Vec2 v)
{
	return getNormalized()*v.getNormalized();
}
float Vec2::getModule(void)
{
	return std::sqrt(x*x+y*y);
}
void Vec2::normalize(void)
{
    float l = this->getModule();
    if ( l != 0 )
    {
        *this /= l;
    }
}
Vec2 Vec2::getNormalized(void)
{
    Vec2 tmp = *this;
    tmp.normalize();
    return tmp;
}
	
//	class Vec3
Vec3::Vec3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

// class Edge2D
Edge2D::operator std::string(void)
{
	std::ostringstream oss;
	oss<<"[ "<<(std::string)vertices[0]<<"_"<<(std::string)vertices[1]<<" ]";
	return oss.str();
}
	
//	class Triangle2D
Triangle2D::Triangle2D()
{

}
Triangle2D::Triangle2D(Point2D a, Point2D b, Point2D c, NormalDir nd) ///creates a triangle with the specified normal
{
	this->a = a;
	this->b = b;
	this->c = c;
	
	calcOrientation();
	changeOrientation(nd);
}
Triangle2D::Triangle2D(Point2D a, Point2D b, Point2D c) ///creates a triangle as specified
{
	this->a = a;
	this->b = b;
	this->c = c;
	calcOrientation();
}
Triangle2D::operator std::string(void)
{
	std::ostringstream oss;
	oss<<"[ "<<(std::string)a<<"_"<<(std::string)b<<"_"<<(std::string)c<<" ]";
	return oss.str();
}
void Triangle2D::calcOrientation(void)
{
	nd = Point2D::getNormalDir(a,b,c);
}
NormalDir Triangle2D::getNormalDir()
{
	return Point2D::getNormalDir(a,b,c);
}
void Triangle2D::changeOrientation(NormalDir nd)
{
	if (this->nd == nd)
	{
		return;
	}
	Point2D tmp = b;
	b = c;
	c = tmp;
	this->nd = nd;
}
bool Triangle2D::isPointInside(Point2D p)
{
	return whereIsPoint(p)==INSIDE;
}
bool Triangle2D::isPointOutside(Point2D p)
{
	return whereIsPoint(p)==OUTSIDE;
}
bool Triangle2D::isPointOnBorder(Point2D p)
{
	return whereIsPoint(p)==ONBORDER;
}
PRP Triangle2D::whereIsPoint(Point2D p)
{
	NormalDir ab,bc,ca;
	ab = Point2D::getNormalDir(a,b,p);
	bc = Point2D::getNormalDir(b,c,p);
	ca = Point2D::getNormalDir(c,a,p);
	NormalDir up = UP, down = DOWN;
	if (nd==DOWN)
	{
		up = DOWN;
		down = UP;
	}
	if (ab==up)
	{
		if (bc==up)
		{
			if (ca==up)
			{
				return INSIDE;
			} else
			if (ca==down)
			{
				return OUTSIDE;
			} else
			{
				return ONBORDER; // on CA
			}
		} else
		if (bc==down)
		{
			return OUTSIDE;
		} else
		{
			if (ca==up)
			{
				return ONBORDER; // on BC
			} else
			if (ca==down)
			{
				return OUTSIDE;
			} else
			{
				return ONBORDER; // on C
			}
		}
	} else
	if (ab==down)
	{
		return OUTSIDE;
	} else
	{
		if (bc==NONE)
		{
			return ONBORDER; // on B
		} else
		if (bc==down)
		{
			return OUTSIDE;
		} else
		{
			if (ca==NONE)
			{
				return ONBORDER; // on A
			} else
			if (ca==up)
			{
				return ONBORDER; // on AB
			} else
			{
				return OUTSIDE;
			}
		}
	}
}
		
Point2D Triangle2D::getA()
{
	return a;
}
Point2D Triangle2D::getB()
{
	return b;
}
Point2D Triangle2D::getC()
{
	return c;
}
		
Point2D Triangle2D::setA(Point2D a)
{
	this->a = a;
	calcOrientation();
}
Point2D Triangle2D::setB(Point2D b)
{
	this->b = b;
	calcOrientation();
}
Point2D Triangle2D::setC(Point2D c)
{
	this->c = c;
	calcOrientation();
}

//	class Polygon2D
Polygon2D::Polygon2D()
{
	nd = NONE;
	size = 0;
	max_size = 0;
	fixed_size = false;
	vertices_a = NULL;
}
Polygon2D::Polygon2D(int size)
{
	nd = NONE;
	this->size = 0;
	max_size = size;
	fixed_size = true;
	vertices_a = new Point2D[max_size];
}
Polygon2D::~Polygon2D(void)
{
	if (vertices_a!=NULL)
	{
		delete [] vertices_a;
	}
}
void Polygon2D::calcOrientation()
{
	float angle = 0;
	float barrier = PI_F*(float)(vertices.getCount()-2);
	DoubleLinkedCircularList<Point2D>::iterator vit = vertices.head();
	bool looped = false;
	while (!looped)
	{
		Point2D a,b,c;
		a = *vit;
		b = *(vit+1);
		c = *(vit+2);
		angle += Point2D::getAngle(a,b,c);
		++vit;
		if (vit==vertices.head())
		{
			looped = true;
		}
	}
	nd = equalWithinEpsilon(angle,barrier,SIN_EPS)?UP:DOWN;
}
Polygon2D::operator std::string(void)
{
	std::ostringstream oss;
	DoubleLinkedCircularList<Point2D>::iterator vit;
	oss<<"[ ";
	int loops = 0;
	for (vit = vertices.head() ; loops==0 ; )
	{
		oss<<(std::string)*vit<<"_";
		vit++;
		if (vit==vertices.head())
		{
			loops++;
		}
	}
	oss.seekp(oss.tellp()-1);
	oss<<" ]";
	return oss.str();
}
bool Polygon2D::isSizeFixed(void)
{
	return fixed_size;
}
void Polygon2D::fixSize(void)
{
	if (fixed_size)
	{
		return;
	}
	vertices_a = new Point2D[size];
	max_size = size;
	int i = 0;
	for (DoubleLinkedCircularList<Point2D>::iterator it = vertices.head() ; vertices.getCount()!=0 ; it.deleteAndNext())
	{
		vertices_a[i] = *it;
		i++;
	}
	fixed_size = true;
}
int Polygon2D::getSize(void)
{
	if (fixed_size)
	{
		return size;
	}
	return vertices.getCount();
}
NormalDir Polygon2D::getNormalDir()
{
	if (nd==NONE)
	{
		calcOrientation();
	}
	return nd;
}
void Polygon2D::changeOrientation(NormalDir nd)
{
	if (nd==NONE)
	{
		calcOrientation();
	}
	if (this->nd == nd)
	{
		return;
	}
	if (fixed_size)
	{
		Point2D tmp;
		for (int i = 0 ; i<size/2 ; i++)
		{
			tmp = vertices_a[i];
			vertices_a[i] = vertices_a[size-i-1];
			vertices_a[size-i-1] = tmp;
		}
	} else
	{
		vertices.reverse();
	}
	this->nd = nd;
}
void Polygon2D::addVertex(Point2D p)
{
	if ((vertices.getCount()!=0 && *(vertices.head()+1)==p))
	{
		return;
	}
	if (fixed_size)
	{
		if (size<max_size)
		{
			vertices_a[size] = p;
		} else
		{
			return;
		}
	} else
	{
		vertices.head().insertPrev(p);
	}
	size++;
}
ITrMesh2D& Polygon2D::triangulate(ITrMesh2D &trm)
{
	Point2D v1,v2,v3;
	fixSize();
//	DoubleLinkedCircularList<Point2D> vs (vertices);
	DoubleLinkedCircularList<Point2D> vs (vertices_a, size);
	DoubleLinkedCircularList<Point2D>::iterator vit = vs.head();
	v1 = *vit;

	while(vs.getCount()>3)
	{
		std::cout<<"WHILE LOOP..."<<(std::string)*vit<<std::endl;
		
		v2 = *(vit+1);
		v3 = *(vit+2);
		std::cout<<"testing: "<<(std::string)Triangle2D(v1,v2,v3)<<"..."<<std::endl;
		if (Point2D::getNormalDir(v1,v2,v3)==UP)
		{
			Triangle2D t (v1,v2,v3);
			std::cout<<"  "<<(std::string)t<<std::endl;
			DoubleLinkedCircularList<Point2D>::iterator vit2 = vit+3;
			bool can_trim = true;
			while(vit2!=vit)
			{
				std::cout<<"	WHILE LOOP... "<<(std::string)*vit2<<std::endl;
				//if (!t.isPointOutside(*vit2))
				if (t.isPointInside(*vit2))
				{
					std::cout<<"found a point inside the trinangle!"<<std::endl;
					v1 = v2;
					++vit;
					can_trim = false;
					break;
				}
				++vit2;
			}
			std::cout<<"	endWHILE LOOP "<<(std::string)*vit2<<std::endl;
			if (can_trim)
			{
				trm.addTriangle(t);
				vit.deleteNext();
				vit--;
				v1 = *vit;
			}
		} else
		{
			++vit;
			v1 = *vit;
		}
	}
	Point2D p2 = *++vit;
	Point2D p3 = *++vit;
	trm.addTriangle(Triangle2D(v1,p2,p3));
//	trm.addTriangle(Triangle2D(v1,*++vit,*++vit)); /// si prende i valori dopo aver calcolato tutto => sbagliato
	return trm;
}

//class TrMesh_DumbPrint : public ITrMesh
TrMesh_DumbPrint::TrMesh_DumbPrint(void)
{

}
ITrMesh2D_Vertex& TrMesh_DumbPrint::addPoint(Point2D p)
{
	std::cout<<"addPoint("<<(std::string)p<<")"<<std::endl;
}
ITrMesh2D_Triangle& TrMesh_DumbPrint::addTriangle(Triangle2D t)
{
	std::cout<<"addTriangle("<<(std::string)t<<")"<<std::endl;
}
QRes TrMesh_DumbPrint::removePoint(Point2D p)
{
	std::cout<<"removePoint("<<(std::string)p<<")"<<std::endl;
}
bool TrMesh_DumbPrint::isTherePoint(Point2D p) const
{
	std::cout<<"isTherePoint("<<(std::string)p<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
ITrMesh2D_Vertex& TrMesh_DumbPrint::getVertex(Point2D p) const
{
	std::cout<<"getVertex("<<(std::string)p<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
ITrMesh2D_Edge& TrMesh_DumbPrint::getEdge(Edge2D e) const
{
	std::cout<<"getEdge("<<(std::string)e<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
ITrMesh2D_Triangle& TrMesh_DumbPrint::getTriangle(Triangle2D t) const
{
	std::cout<<"getTriangle("<<(std::string)t<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getVertices() const
{
	std::cout<<"getVertices(): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getVertices(const ITrMesh2D_Vertex& v) const
{
	std::cout<<"getVertices("<<(std::string)v.getVertex()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getVertices(const ITrMesh2D_Edge& e) const
{
	std::cout<<"getVertices("<<(std::string)e.getEdge()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getVertices(const ITrMesh2D_Triangle& t) const
{
	std::cout<<"getVertices("<<(std::string)t.getTriangle()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getEdges() const
{
	std::cout<<"getEdges(): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getEdges(const ITrMesh2D_Vertex& v) const
{
	std::cout<<"getEdges("<<(std::string)v.getVertex()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getEdges(const ITrMesh2D_Edge& e) const
{
	std::cout<<"getEdges("<<(std::string)e.getEdge()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getEdges(const ITrMesh2D_Triangle& t) const
{
	std::cout<<"getEdges("<<(std::string)t.getTriangle()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getTriangles() const
{
	std::cout<<"getTriangles(): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getTriangles(const ITrMesh2D_Vertex& v) const
{
	std::cout<<"getTriangles("<<(std::string)v.getVertex()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getTriangles(const ITrMesh2D_Edge& e) const
{
	std::cout<<"getTriangles("<<(std::string)e.getEdge()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}
std::list<ITrMesh2D_Vertex> TrMesh_DumbPrint::getTriangles(const ITrMesh2D_Triangle& t) const
{
	std::cout<<"getTriangles("<<(std::string)t.getTriangle()<<"): ?(TrMesh_DumbPrint do not stores any data)"<<std::endl;
}

//	class TrMesh_TrBased : public ITrMesh2D


























