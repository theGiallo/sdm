#ifndef LIB_GMLUTILITIES_H
#define LIB_GMLUTILITIES_H
#include "libGeometry.h"
#include <iostream>
#include <sstream>

namespace GMLUtilities
{
	/**
	 * Read functions
	 **/
	libGeometry::Polygon2D& readPolygonFromGML(char* path);
	bool openFile(std::string filename, char* out_features_name);
	void closeFileAfterRead(void);
	bool readStartFeatureWr(char* in_attrib_name, int* type);
	bool readPolStart(char* in_attrib_name, int type);
	bool readTriangle(libGeometry::Triangle2D& out_tr, char* in_attrib_name, int type); /// type can be LINES or POLYGONS
	char* readAttribName(std::string filename, int id, char* out_name);
	void readBoundaries(libGeometry::Vec2& bl, libGeometry::Vec2& tr);
	
	/**
	 * Write functions
	 **/
	void writeSingleTriangle(libGeometry::Triangle2D t);
	void startFeature(std::string name, int id);
	void endFeature(std::string name, std::string attr_name, int id);
	void openFile(std::string filename, std::string heading, libGeometry::Vec2 bl, libGeometry::Vec2 tr);
	void writeSchema(std::string filename, int entityType, std::string heading, std::string start);
	void closeFile();
	void writeLine(libGeometry::Vec2 start, libGeometry::Vec2 end);
	void writeTrianglesGML(libGeometry::ITrMesh2D& trm, std::string filename);
	void writeTrianglesAdjacentiesGML(libGeometry::ITrMesh2D& trm, std::string filename);
}
#endif
