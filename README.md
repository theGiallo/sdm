# SDM

Partial implementation of an indexed data structure for a 2D triangulation with an helper to load and recreate TT relation. Functionalities to load and save a triangulation from/to a GML file.

Made for the accademic course Spatial Data Management at DISI (now DIBRIS), University of Genoa, Italy.

Author: Gianluca Alloisio <http://thegiallo.blogspot.com>

Teacher: Paola Magillo <http://www.disi.unige.it/person/MagilloP/>
