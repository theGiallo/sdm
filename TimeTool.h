/* 
 * File:   TimeTool.h
 * Author: thegiallo
 *
 * Created on 9 ottobre 2011, 18.32
 */

#ifndef TIMETOOL_H
#define	TIMETOOL_H

//#include <SDL/SDL.h>
#ifdef WIN32
#include <Windows.h>
#else
#include <sys/time.h>
#include <ctime>
#include <sys/types.h>
#endif
#ifndef Uint8
#define Uint8 unsigned char
#endif
#ifndef int64_t
typedef long int int64_t;
#endif

typedef Uint8 CheckType;
#define START_CHECK 0x0
#define STOP_CHECK 0x1
#define MIDDLE_CHECK 0x2
#define PAUSE_CHECK 0x3
#define UNPAUSE_CHECK 0x3

class TimeTool {
public:
    TimeTool();
    
    bool calc_average;
    int64_t last_check;
    int64_t last_delta;
    int64_t partial_delta;
    int64_t all_average;
    int64_t average_counts;
    
    void resetAverageCount();
    void startAverageCount();
    void pauseAverageCount();
    void stopAverageCount(); /// pause and reset
    int64_t getAverage();
    
    /**
     * @return the amount of time elapsed from the last check in microseconds
     */
    int64_t check(CheckType t=MIDDLE_CHECK);

    int64_t getTimeMicroSeconds64();
    
    
private:

};

#endif	/* TIMETOOL_H */

