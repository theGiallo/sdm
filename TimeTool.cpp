/* 
 * File:   TimeTool.cpp
 * Author: thegiallo
 * 
 * Created on 9 ottobre 2011, 18.32
 */

#include "TimeTool.h"
#include <iostream>

TimeTool::TimeTool()
{    
    calc_average = true;
    last_check = 0;
    last_delta = 0;
    all_average = 0;
    average_counts = 0;
    partial_delta = 0;
}
void TimeTool::resetAverageCount()
{
    all_average = 0;
    average_counts = 0;
}
void TimeTool::startAverageCount()
{
    calc_average = true;
}
void TimeTool::pauseAverageCount()
{
    calc_average = false;
}
void TimeTool::stopAverageCount()
{
    pauseAverageCount();
    resetAverageCount();
}
int64_t TimeTool::getAverage()
{
    return all_average;
}
int64_t TimeTool::check(CheckType t)
{
    int64_t new_check = getTimeMicroSeconds64();
    if (t==STOP_CHECK || t==MIDDLE_CHECK)
    {
        last_delta = partial_delta + new_check-last_check;
        if (calc_average)
        {
            all_average = all_average + (last_delta-all_average)/(long double)(++average_counts);
        }
        partial_delta = 0;
    }
    if (t==PAUSE_CHECK)
    {
        partial_delta += new_check-last_check;
    }
    if (t==START_CHECK || t==MIDDLE_CHECK || t==UNPAUSE_CHECK)
    {
        last_check = new_check;
    }
    return last_delta;
}



/**
 * Returns the amount of microseconds elapsed since the UNIX epoch. Works on both
 * windows and linux.
 **/
int64_t TimeTool::getTimeMicroSeconds64()
{
#ifdef WIN32
    /* Windows */
    FILETIME ft;
    LARGE_INTEGER li;

    /**
     * Get the amount of 100 nano seconds intervals elapsed since January 1, 1601 (UTC) and copy it
     * to a LARGE_INTEGER structure.
     **/
    GetSystemTimeAsFileTime(&ft);
    li.LowPart = ft.dwLowDateTime;
    li.HighPart = ft.dwHighDateTime;

    int64_t ret = li.QuadPart;
    ret -= 116444736000000000LL; /// Convert from file time to UNIX epoch time.
    return ret/10; ///From 100 nano seconds (10^-7) to 1 millisecond (10^-3) intervals
#else
    /* Linux */
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_usec+tv.tv_sec*1000000L;
#endif
}