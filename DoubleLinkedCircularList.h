#ifndef DOUBLELINKEDCIRCULARLIST_H
#define	DOUBLELINKEDCIRCULARLIST_H

#include <iterator>
#include <iostream>

template <class T>
class DoubleLinkedCircularList
{
public:
	class Element
	{
	public:
		Element *_next, *_prev;
		T el;
		Element(T val)
		{
			el = val;
		}
		Element* next()
		{
			return _next;
		}
		Element* prev()
		{
			return _prev;
		}
		Element* insertNext(Element* nel)
		{
			if (nel==NULL)
			{
				throw false;
			}
			nel->_next = this->_next;
			this->_next = nel;
			if (nel->_next!=NULL)
			{
				nel->_next->_prev = nel;
			}
			nel->_prev = this;
			return nel;
		}
		Element* insertPrev(Element* nel)
		{
			if (nel==NULL)
			{
				throw false;
			}
			nel->_prev = this->_prev;
			this->_prev = nel;
			if (nel->_prev!=NULL)
			{
				nel->_prev->_next = nel;
			}
			nel->_next = this;
			return nel;
		}
		Element* removeNext(void)
		{
			if (_next==NULL)
			{
				return NULL;
			}
			Element* tmp = _next;
			_next = _next->_next;
			delete tmp;
			return _next;
		}
		Element* removePrev(void)
		{
			if (_prev==NULL)
			{
				return NULL;
			}
			Element* tmp = _prev;
			_prev = _prev->_prev;
			delete tmp;
			return _prev;
		}
	};
	class iterator
	{
	protected:
		Element* curr;
		DoubleLinkedCircularList* dlcl;
	public:
		iterator(DoubleLinkedCircularList* l=NULL, Element* curr=NULL)
		{
			this->dlcl = l;
			if (curr==NULL && l!=NULL)
			{
				this->curr = l->_head;
				return;
			}
			this->curr = curr;
		}
		iterator operator ++(void) /// ++it
		{
			if (curr!=NULL)
			{
				this->curr = curr->next();
			}
			return *this;
		}
		iterator operator ++(int i) /// it++
		{
			iterator tmp = *this;
			if (curr!=NULL)
			{
				this->curr = curr->next();
			}
			return tmp;
		}
		iterator operator --(void)
		{
			if (curr!=NULL)
			{
				this->curr = curr->prev();
			}
			return *this;
		}
		iterator operator --(int i)
		{
			iterator tmp = *this;
			if (curr!=NULL)
			{
				this->curr = curr->prev();
			}
			return tmp;
		}
		iterator operator +(int i)
		{
			iterator res=*this;
			for (; i>0 ; i--)
			{
				++res;
			}
			for (; i<0 ; i++)
			{
				--res;
			}
			return res;
		}
		iterator operator -(int i)
		{
			return (*this)+(-i);
		}
		bool operator ==(iterator it)
		{
			return curr==it.curr;
		}
		bool operator !=(iterator it)
		{
			return curr!=it.curr;
		}
		T& operator*(void)
		{
			if (curr!=NULL)
			{
				return curr->el;
			}
			throw false;
		}
		iterator insertNext(T nval)
		{
			dlcl->count++;
			Element* nel = new Element(nval);
			if (curr==NULL)
			{
				dlcl->_head = nel;
				curr = nel;
				nel->_next=nel->_prev=nel;
				return *this;
			}
			curr->insertNext(nel);
			return *this;
		}
		iterator insertPrev(T nval)
		{
			dlcl->count++;
			Element* nel = new Element(nval);
			if (curr==NULL)
			{
				dlcl->_head = nel;
				curr = nel;
				nel->_next=nel->_prev=nel;
				return *this;
			}
			curr->insertPrev(nel);
			return *this;
		}
		iterator deleteNext(void)
		{
			if (curr==NULL)
			{
				return * this;
			}
			dlcl->count--;
			Element* tmp = curr->_next;
			if (dlcl->count==0)
			{
				curr = NULL;
				delete tmp;
				return *this;
			}
			curr->_next = tmp->_next;
			curr->_next->_prev = curr;
			delete tmp;
			return *this;
		}
		iterator deletePrev(void)
		{
			if (curr==NULL)
			{
				return * this;
			}
			dlcl->count--;
			Element* tmp = curr->_prev;
			if (dlcl->count==0)
			{
				curr = NULL;
				delete tmp;
				return *this;
			}
			curr->_prev = tmp->_prev;
			curr->_prev->_next = curr;
			delete tmp;
			return *this;
		}
		iterator deleteAndNext(void)
		{
			iterator res = (*this)+1;
			res.deletePrev();
			*this = res;
			return res;
		}
		iterator deleteAndPrev(void)
		{
			iterator res = (*this)-1;
			res.deleteNext();
			*this = res;
			return res;
		}
	};
	iterator head(void)
	{
		return iterator(this);
	}
	DoubleLinkedCircularList(void)
	{
		_head = NULL;
		count=0;
	}
	DoubleLinkedCircularList(DoubleLinkedCircularList& l)
	{
		this->_head = NULL;
		count = 0;
		iterator it, me;
		me = head();
		int i = 0;
		int c = l.getCount();
		for (it=l.head() ; i<c ; i++)
		{
//			std::cout<<(std::string)*it<<std::endl;
			me.insertNext(*it);
			me++;
			it++;
		}
	}
	DoubleLinkedCircularList(T* array, int l)
	{
		this->_head = NULL;
		count = 0;
		iterator it, me;
		me = head();
		for (int i = 0 ; i<l ; i++)
		{
			me.insertNext(array[i]);
			me++;
		}
	}
	unsigned int getCount() const
	{
		return count;
	}
	void reverse()
	{
		Element *old_head, *curr, *tmp;
		old_head = _head;
		_head = _head->_prev;
		curr = old_head;
		tmp = curr->_prev;
		curr->_prev = curr->_next;
		curr->_next = tmp;
		for (curr = curr->_prev ; curr != old_head ; curr = curr->_prev )
		{
			tmp = curr->_prev;
			curr->_prev = curr->_next;
			curr->_next = tmp;
		}
	}
	void clear(void)
	{
		iterator it = head();
		while (_head!=NULL)
		{
			it.deleteAndNext();
		}
	}
protected:
	Element* _head;
	unsigned int count;
};
#endif /* DOUBLELINKEDCIRCULARLIST_H */
