/*
Universita' degli Studi di Genova;
Corso di Laurea Magistrale in Informatica;
Corso di Spatial Data Management (SDM);
Docente Paola Magillo;
Utilita' per scrivere un file in formato GML.
*/

#ifndef SDM_2012_WRITE_GML_H
#define SDM_2012_WRITE_GML_H

#include <stdio.h>

/********************************** MACRO **********************************/

/* Tipi possibili di geometria per un'entita' */
#define POINTS 0
#define LINES 1
#define POLYGONS 2

/**************************** GESTIONE DEI FILE ****************************/

extern void openSchemaFile(char * fileName);
extern void closeSchemaFile();

extern void openEntityFile(char * fileName);
extern void closeEntityFile();

/************************ SCRITTURA DEL FILE SCHEMA ************************/

/* Da chiamare per primo */
extern void writeSchemaHeading(int entityType, char * name);

/* Da chiamare per secondo */
extern void writeSchemaGeneralStart(int entityType, char * name);

/* Da chiamare per terzi, uno per ogni attributo,
   secondo il tipo, questo per numero decimale */
extern void writeSchemaDecimalAttrib(char * name, int tot_digit, int dec_digit);

/* Da chiamare per terzi, uno per ogni attributo,
   secondo il tipo, questo per numero intero */
extern void writeSchemaIntegerAttrib(char * name, int tot_digit);

/* Da chiamare per terzi, uno per ogni attributo,
   secondo il tipo, questo per stringa */
extern void writeSchemaStringAttrib(char * name, int max_len);

/* Da chiamare per penultimo */
extern void writeSchemaGeneralEnd();

/* Da chiamare per ultimo */
extern void writeSchemaClosing();

/************************* SCRITTURA DEL FILE DATI *************************/

/* Da chiamare per primo */
extern void writeHeading(char * name);

/* Da chiamare per secondo */
extern void writeBounds(double xmin, double ymin, double xmax, double ymax);
  
/* da chiamare all'inizio di ogni feature */
extern void writeStartFeature(char * name, int count);

/* da chiamare per ogni feature la specifica della geometria (ved. dopo) */

/* da chiamare per ogni feature la specifica degli attributi (ved. dopo) */

/* da chiamare alla fine di ogni feature */
extern void writeEndFeature(char * name);

/* da chiamare per ultimo */
extern void writeClosing();

/*************************** GEOMETRIA E ATTRIBUTI *************************/

/* geometria punto */
extern void writePointGeom(double x, double y);

/* geometria polyline tutta assieme da array */
extern void writePolylineGeom(int len, double * x, double * y);

/* geometria poligono tutta assieme da array */
extern void writePolygonGeom(int len, double * x, double * y);

/* geometria polyline e poligono inizio */
extern void writePolylineStart();
extern void writePolygonStart();

/* geometria polyline o poligono un vertice */
extern void writePolyVertex(double x, double y);

/* geometria polyline o poligono fine */
extern void writePolylineEnd();
extern void writePolygonEnd();

/* scrittura attributi - attributo intero */
extern void writeIntAttrib(char * name, int value);

/* scrittura attributi - attributo stringa */
extern void writeStringAttrib(char * name, char * value);

/* scrittura attributi - attributo numero decimale */
extern void writeDecimalAttrib(char * name, double value);

#endif /* SDM_2012_WRITE_GML_H */
