#ifndef MEMORYTOOL_H
#define MEMORYTOOL_H

#include <proc/readproc.h>

class MemoryTool
{
	protected:
	unsigned long zero_vm;
	unsigned long min_vm;
	unsigned long max_vm;
	unsigned long average_vm;
	long int count_vm;
	
	long int zero_drt;
	long int min_drt;
	long int max_drt;
	long double average_drt;
	long int count_drt;
	public:
	MemoryTool(void);
	unsigned long getVirtMem(void); /// returns the virtual memory of the process in bytes
	unsigned long getMemDirtyMode(void); /// returns the memory calculated in dirty mode of the process in kbytes
	
	unsigned long checkVirtMem(void); /// returns the virtual memory of the process in bytes and updates min, max and average
	unsigned long getMinVirtMem(void);
	unsigned long getMaxVirtMem(void);
	unsigned long getAvrgVirtMem(void);
	unsigned long checkMemDirty(void); /// returns the memory calculated in dirty mode of the process in kbytes and updates min, max and average
	unsigned long getMinMemDirty(void);
	unsigned long getMaxMemDirty(void);
	long double getAvrgMemDirty(void);
	void reset(void);
	void setZeroVM(unsigned long zero_vm);
	void setZeroDirty(unsigned long zero_drt);
};
#endif //MEMORYTOOL_H
