
#DEBUG = -g -DDEBUG_TRIANGULATEPOLYGON -DDEBUGPRQTREES -DDEBUG
#DEBUG = -g -DDEBUGPRQTREES
#DEBUG = -g -DDEBUG
CPPC = g++-4.4

parte1: mainParte1.o libGeometry.o GMLUtilities.o readGML.o writeGML.o
	clear
	$(CPPC) mainParte1.o libGeometry.o GMLUtilities.o readGML.o writeGML.o -o parte1 -Wfatal-errors $(DEBUG)
	clear

parte2: mainParte2.o libGeometry.o GMLUtilities.o readGML.o writeGML.o TimeTool.o MemoryTool.o
	clear
	$(CPPC) mainParte2.o libGeometry.o GMLUtilities.o readGML.o writeGML.o TimeTool.o MemoryTool.o -o parte2 -Wfatal-errors $(DEBUG)  -lprocps
	clear

mainParte1.o: mainParte1.cpp
	clear
	$(CPPC) mainParte1.cpp -c $(DEBUG)
	clear

mainParte2.o: mainParte2.cpp
	clear
	$(CPPC) mainParte2.cpp -c $(DEBUG)
	clear

readGML.o: readGML.c readGML.h
	clear
	$(CPPC) readGML.c -c -fpermissive $(DEBUG)
	clear

writeGML.o: writeGML.c writeGML.h
	clear
	$(CPPC) writeGML.c -c -fpermissive $(DEBUG)
	clear

libGeometry.o: libGeometry.cpp libGeometry.h DoubleLinkedCircularList.h
	clear
	$(CPPC) libGeometry.cpp -c -Wfatal-errors $(DEBUG) -Wall
	clear

GMLUtilities.o: GMLUtilities.cpp GMLUtilities.h readGML.o writeGML.o libGeometry.o
	clear
	$(CPPC) GMLUtilities.cpp -c $(DEBUG)
	clear

TimeTool.o: TimeTool.cpp TimeTool.h
	clear
	$(CPPC) TimeTool.cpp -c $(DEBUG)
	clear
	
MemoryTool.o: MemoryTool.cpp MemoryTool.h
	clear
	$(CPPC) MemoryTool.cpp -c $(DEBUG)
	clear

clear:
	clear
	rm libGeometry.o readGML.o writeGML.oGMLUtilities.o TimeTool.o MemoryTool.o mainParte1.o mainParte2.o
	clear;
