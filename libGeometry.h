#ifndef LIB_GEOMETRY_H
#define LIB_GEOMETRY_H

#include <cstdlib>
#include <string>
#include <list>
#include <cmath>
#include "DoubleLinkedCircularList.h"

/**
 * IMPORTANT: catch and delete if not necessary all reference returns
 **/

namespace libGeometry
{
	/**
	 * Polygons with up normal are CCW ones.
	 **/

	// constants
	const float PI_F =  3.14159f;
	const float SIN_EPS = 1e-8f;
	
	bool equalWithinEpsilon(float v1, float v2, float eps);
	
	enum NormalDir
	{
		UP,
		DOWN,
		NONE
	};
	
	enum PRP /// PointRelativePosition
	{
		OUTSIDE,
		INSIDE,
		ONBORDER
	};
	
	enum QRes
	{
		DONE,
		ALREADY_THERE,
		NOT_THERE
	};
	
	class Point2D;
	class Vec2;
	class Vec3;
	class Edge2D;
	class Triangle2D;
	class Polygon2D;
	class ITrMesh2D;
	class ITrMesh2D_Vertex;
	class ITrMesh2D_Edge;
	class ITrMesh2D_Triangle;
	class TrMesh2D_DumbPrint;
	class TrMesh2D_TrBased;
	class TrMesh2D_Vertex_TrBased;
	class TrMesh2D_Triangle_TrBased;
	class TrMesh2D_TrBased_internalVertex;
	class TrMesh2D_TrBased_internalTriangle;
	class ITrMesh2D_TrBased_NewEntriesManager;
	
	
	
	class Point2D
	{
	public:
		float x,y;
		
		Point2D(void);
		Point2D(float x, float y);
		
		operator Vec2(void) const;
		bool operator == (Point2D p) const;
		operator std::string(void) const;
		
		static float getAngle(Point2D a, Point2D b, Point2D c); /// returns the angle abcexpressed in radians
		static float getSin(Point2D a, Point2D b, Point2D c);
		static float getCos(Point2D a, Point2D b, Point2D c);
		static float getNormalModule(Point2D a, Point2D b, Point2D c);
		static NormalDir getNormalDir(Point2D a, Point2D b, Point2D c);
	};
	
	class Point2D_int
	{
	public:
		int x,y;
		Point2D_int(void);
		Point2D_int(int x, int y);
		
		operator Point2D(void) const;
		bool operator == (Point2D_int p) const;
		operator std::string(void) const;
	};
	
	class Vec2
	{
	public:
		float x,y;
		
		Vec2(void);
		Vec2(float x, float y);
		Vec2(Point2D a, Point2D b); /// creates the vector b-a (from a to b)
		
		operator Point2D(void) const;
		operator Vec3(void) const;
		bool operator == (Vec2 v) const;
		Vec3 operator ^ (Vec2 v) const;
	    float operator * (Vec2 v) const;
	    Vec2 operator + (Vec2 v) const;
	    Vec2 operator - (Vec2 v) const;
		Vec2 operator/(float k) const;
		Vec2 operator/=(float k);
		operator std::string(void) const;
		
		float getAngle(Vec2 v) const; /// returns the angle between the vectors expressed in radians
		float getSin(Vec2 v) const;
		float getCos(Vec2 v) const;
		
		float getModule(void) const;
		void normalize(void);
		Vec2 getNormalized(void) const;
	};
	
	class Vec3
	{
	public:
		float x,y,z;
		
		Vec3(float x, float y, float z);
		operator std::string(void) const;
	};
	
	class Edge2D
	{
	public:
		Point2D vertices[2];
		operator std::string(void) const;
		Point2D getCentroid(void) const;
	};
	
	class Triangle2D
	{
	private:
		Point2D a,b,c;
		NormalDir nd;
		void calcOrientation(void);
		
	public:
		Triangle2D(void);
		Triangle2D(Point2D a, Point2D b, Point2D c, NormalDir nd); ///creates a triangle with the specified normal
		Triangle2D(Point2D a, Point2D b, Point2D c); ///creates a triangle with the specified normal
		
		operator std::string(void);
		
		NormalDir getNormalDir(void);
		void changeOrientation(NormalDir nd);
		
		bool isPointInside(Point2D p);
		bool isPointOutside(Point2D p);
		bool isPointOnBorder(Point2D p);
		PRP whereIsPoint(Point2D p);
		
		Vec2 getCentroid(void) const;
		
		Point2D getA(void);
		Point2D getB(void);
		Point2D getC(void);
		
		Point2D setA(Point2D a);
		Point2D setB(Point2D b);
		Point2D setC(Point2D c);
	};
	
	class Polygon2D
	{
	private:
		NormalDir nd;
		bool fixed_size;
		int size;
		int max_size;
	public:
		void calcOrientation(void);
		DoubleLinkedCircularList<Point2D> vertices;
		Point2D* vertices_a;
		
		Polygon2D(void);
		Polygon2D(int size);
		~Polygon2D(void);
		
		bool isSizeFixed(void);
		void fixSize(void);

		operator std::string(void);
		
		int getSize(void);
		
		NormalDir getNormalDir(void);
		void changeOrientation(NormalDir nd);

		void addVertex(Point2D p);
		ITrMesh2D& triangulate(ITrMesh2D &trm);
	};
	
	class ITrMesh2D
	{
	public:
		virtual ITrMesh2D_Vertex& addPoint(Point2D p)=0;
		virtual ITrMesh2D_Triangle& addTriangle(Triangle2D t)=0;
		virtual ITrMesh2D_Triangle& addTriangle(Triangle2D t, ITrMesh2D_Triangle** TT)=0; /// TT must be an array[3] of ITrMesh2D_Triangle*, NULL if none or unknown
		virtual QRes removePoint(Point2D p)=0;
		virtual bool isTherePoint(Point2D p)const =0;
		virtual ITrMesh2D_Vertex& getVertex(Point2D p)const =0;
		virtual ITrMesh2D_Edge& getEdge(Edge2D e)const =0;
		virtual ITrMesh2D_Triangle& getTriangle(Triangle2D t)const =0;
		virtual std::list<ITrMesh2D_Vertex*> getVertices()const =0;
		virtual std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Vertex& v)const =0;
		virtual std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Edge& v)const =0;
		virtual std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Triangle& t)const =0;
		virtual std::list<ITrMesh2D_Edge*> getEdges()const =0;
		virtual std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Vertex& v)const =0;
		virtual std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Edge& v)const =0;
		virtual std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Triangle& t)const =0;
		virtual std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Vertex& v)const =0;
		virtual std::list<ITrMesh2D_Triangle*> getTriangles()const =0;
		virtual std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Edge& v)const =0;
		virtual std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Triangle& t)const =0;
		
		virtual Vec2 getBottomLeftBoundary(void) const =0;
		virtual Vec2 getTopRightBoundary(void) const =0;
	};
	
	class ITrMesh2D_Vertex
	{
	public:
		virtual Point2D getVertex(void)const =0;
	};
	class ITrMesh2D_Edge
	{
	public:
		virtual Edge2D getEdge(void)const =0;
	};
	class ITrMesh2D_Triangle
	{
	public:
		virtual Triangle2D getTriangle(void)const =0;
	};
	class PT_aux
	{
	public:
		int p;
		ITrMesh2D_Triangle* t;
		PT_aux(void)
		{
			p = -1;
			t = NULL; 
		}
		PT_aux(int p , ITrMesh2D_Triangle* t)
		{
			this->p = p;
			this->t = t; 
		}
	};
	class TrMesh2D_DumbPrint : public ITrMesh2D
	{
	private:
		static TrMesh2D_Vertex_TrBased v;
		static TrMesh2D_Triangle_TrBased t;
	public:
		TrMesh2D_DumbPrint(void);
		ITrMesh2D_Vertex& addPoint(Point2D p);
		ITrMesh2D_Triangle& addTriangle(Triangle2D t);
		ITrMesh2D_Triangle& addTriangle(Triangle2D t, ITrMesh2D_Triangle** TT);
		QRes removePoint(Point2D p);
		bool isTherePoint(Point2D p) const;
		ITrMesh2D_Vertex& getVertex(Point2D p) const;
		ITrMesh2D_Edge& getEdge(Edge2D e) const;
		ITrMesh2D_Triangle& getTriangle(Triangle2D t) const;
		std::list<ITrMesh2D_Vertex*> getVertices() const;
		std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Vertex& v) const;
		std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Edge& e) const;
		std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Triangle& t) const;
		std::list<ITrMesh2D_Edge*> getEdges() const;
		std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Vertex& v) const;
		std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Edge& e) const;
		std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Triangle& t) const;
		std::list<ITrMesh2D_Triangle*> getTriangles() const;
		std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Vertex& v) const;
		std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Edge& e) const;
		std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Triangle& t) const;
		
		Vec2 getBottomLeftBoundary(void) const;
		Vec2 getTopRightBoundary(void) const;
	};
	
	class TrMesh2D_Vertex_TrBased : public ITrMesh2D_Vertex
	{
	protected:
		Point2D vertex;
		int index;
	public:
		TrMesh2D_Vertex_TrBased(Point2D vertex=Point2D(), int index=-1);
		Point2D getVertex(void)const;
		int getIndex(void) const;
	};
	class TrMesh2D_Triangle_TrBased : public ITrMesh2D_Triangle
	{
	protected:
		Triangle2D t;
		int index;
	public:
		TrMesh2D_Triangle_TrBased(Triangle2D t=Triangle2D(), int index=-1);
		Triangle2D getTriangle(void)const;
		int getIndex(void) const;
	};
	class TrMesh2D_TrBased_internalVertex
	{
	public:
		Point2D vertex;
		int VT_star;
	
		TrMesh2D_TrBased_internalVertex(Point2D vertex=Point2D(), int VT_star = -1);
	};
	class TrMesh2D_TrBased_internalTriangle
	{
	public:
		int TT[3];
		int vertices[3];
		
		TrMesh2D_TrBased_internalTriangle();
		TrMesh2D_TrBased_internalTriangle(int vertices[3]);
	};
	class ITrMesh2D_TrBased_NewEntriesManager
	{
	public:
		class TT
		{
		public:
			int tr_id;
			int tt_id;
			TT(int tr_id=-1, int tt_id=-1) : tr_id(tr_id), tt_id(tt_id){};
		};
		virtual void setOwner(TrMesh2D_TrBased* owner)=0;
		virtual int addingVertex(Point2D p, int new_id)=0; /// returns the id of the vertex previously added or adds the new vertex and returns new_id (that should be the id of the point if it would be added)
		virtual TT addingEdge(Point2D_int e, int tr_id, int tt_id)=0; /// returns a TT with the id of the triangle that owns the edge and the relative id of TT if previously added, tr_id and tt_id if it's new or -1 if error. tr_id is the id of the owner triangle of e. tt is the id of the TT relation based on e for tr_id
	};
	class TrMesh2D_TrBased : public ITrMesh2D
	{
	protected:
		TrMesh2D_TrBased_internalVertex* vertices;
		TrMesh2D_TrBased_internalTriangle* triangles;
		int vertices_count;
		int triangles_count;
		int vertices_maxcount;
		int triangles_maxcount;
		ITrMesh2D_TrBased_NewEntriesManager* new_entries_manager;
		Vec2 bottom_left_expected;
		Vec2 top_right_expected;
		
		int getNewPointID(void) const; /// returns the id that would be given to a new point
		int findVertex(Point2D p) const;
		int insertVertex(Point2D p);
		int insertTriangle(Triangle2D t); ///inserts a triangle without calculating TT relation and returns his id
	public:
		static const int DEF_VERT_NUM = (int)2<<15;
		TrMesh2D_TrBased(int nvert=DEF_VERT_NUM, ITrMesh2D_TrBased_NewEntriesManager* new_entries_manager=NULL); /// new_entries_manager can be null only if you will always use adjacencies suggestions 
		~TrMesh2D_TrBased();
		ITrMesh2D_Vertex& addPoint(Point2D p);
		ITrMesh2D_Triangle& addTriangle(Triangle2D t);
		ITrMesh2D_Triangle& addTriangle(Triangle2D t, ITrMesh2D_Triangle** TT); /// TT if an array of length 3 of suggested adjacencies (ITrMesh2D_Triangle*)
		QRes removePoint(Point2D p);
		bool isTherePoint(Point2D p) const;
		ITrMesh2D_Vertex& getVertex(Point2D p) const;
		ITrMesh2D_Edge& getEdge(Edge2D e) const;
		ITrMesh2D_Triangle& getTriangle(Triangle2D t) const;
		std::list<ITrMesh2D_Vertex*> getVertices() const;
		std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Vertex& v) const;
		std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Edge& e) const;
		std::list<ITrMesh2D_Vertex*> getVertices(const ITrMesh2D_Triangle& t) const;
		std::list<ITrMesh2D_Edge*> getEdges() const;
		std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Vertex& v) const;
		std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Edge& e) const;
		std::list<ITrMesh2D_Edge*> getEdges(const ITrMesh2D_Triangle& t) const;
		std::list<ITrMesh2D_Triangle*> getTriangles() const;
		std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Vertex& v) const;
		std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Edge& e) const;
		std::list<ITrMesh2D_Triangle*> getTriangles(const ITrMesh2D_Triangle& t) const;
		
		Vec2 getBottomLeftBoundary(void) const;
		Vec2 getTopRightBoundary(void) const;
		void setExpectedBottomLeftBoundary(Vec2 bl);
		void setExpectedTopRightBoundary(Vec2 tr);
		Vec2 getExpectedBottomLeftBoundary(void) const;
		Vec2 getExpectedTopRightBoundary(void) const;
		
		Point2D getVertex(int id); /// used by NewEntryManager
		
		int getVerticesCount(void);
		int getTrianglesCount(void);
		int getVerticesMaxCount(void);
		int getTrianglesMaxCount(void);
	};
	class AuxEdge
	{
	public:
		Point2D_int e;
		int tr_id;
		int tt_id;
		AuxEdge(Point2D_int e, int tr_id, int tt_id) : e(e), tr_id(tr_id), tt_id(tt_id){};
	};
	class TrMesh2D_TrBased_GridNewEntriesManager : public ITrMesh2D_TrBased_NewEntriesManager
	{
	protected:
		TrMesh2D_TrBased* owner;
		int rows, cols;
		std::list<int>** vertices_grid;
		std::list<AuxEdge>** edges_pseudo_grid; /// a triangular grid in which a cell [row,col] has always col<=row
		float cell_height;
		float cell_width;
		Point2D origin;
		Point2D top_right;
		int erows; /// the number of rows of edge_pseudo_grid
		int ecells_edge; /// the number of ids contained in a cell of edge_pseudo_grid
		
		int getRow(const Point2D& p); /// returns the id of the row of the point in the grid, -1 if outside
		int getCol(const Point2D& p); /// returns the id of the col of the point in the grid, -1 if outside
		
		int getECoord(int id); /// returns the id of the row/col of the given point id
	public:
		TrMesh2D_TrBased_GridNewEntriesManager(TrMesh2D_TrBased* owner, int rows, int cols, int erows);
		~TrMesh2D_TrBased_GridNewEntriesManager();
		void setOwner(TrMesh2D_TrBased* owner);
		int addingVertex(Point2D p, int new_id); /// returns the id of the vertex previously added or adds the new vertex and returns new_id (that should be the id of the point if it would be added)
		TT addingEdge(Point2D_int, int tr_id, int tt_id); /// returns a TT with the id of the triangle that owns the edge and the relative id of TT if previously added, tr_id and tt_id if it's new or -1 if error. tr_id is the id of the owner triangle of e. tt is the id of the TT relation based on e for tr_id
	};
	class TrMesh2D_TrBased_PRQTreeNewEntriesManager : public ITrMesh2D_TrBased_NewEntriesManager
	{
	protected:
		enum QTCardinals
		{
			NorthWest = 0,
			NorthEast = 1,
			SouthEast = 2,
			SouthWest = 3, /// DO NOT CHANGE THE ABOVE ONES!
			OutNorthWest = 4,		
			OutNorth = 5,
			OutNorthEast = 6,
			OutWest = 7,
			OutEast = 8,
			OutSouthWest = 9,
			OutSouth = 10,
			OutSouthEast = 11
		};
		class QTNode
		{
		protected:
			unsigned char type;
		public:
			static const unsigned char internal_type = 0;
			static const unsigned char leaf_type = 1;
			unsigned char getType()
			{
				return type;
			}
		};
		class QTInternalNode : public QTNode
		{
		public:
			QTInternalNode()
			{
				children[0]=children[1]=children[2]=children[3]=NULL;
				type = QTNode::internal_type;
			};
			QTNode* children[4];
		};
		template <class T>
		class QTLeafNode : public QTNode
		{
		public:
			QTLeafNode(T val):val(val)
			{
				type = QTNode::leaf_type;
			}
			T val;
		};
		
		TrMesh2D_TrBased* owner;
		Point2D bl,tr;
		QTNode* root_points; /// int
		QTNode* root_edges;  /// AuxEdge
		int verticesmaxcount;
		
		int getChildIdNode(Point2D& bl, Point2D& tr, Point2D& p);
		int getChildIdNode(Point2D_int& bl, Point2D_int& tr, Point2D_int& p);
		int calcChildBoundaries(int* child, Point2D* bl, Point2D* tr); //updates the boundaries to the new child value (if child is an out vaule updates it to the next father node)
		void calcFatherBoundaries(int child, Point2D* bl, Point2D* tr);
		int calcChildBoundaries(int* child, Point2D_int* bl, Point2D_int* tr); //updates the boundaries to the new child value
		int getOppositeChild(int child);
		
		static void aux_printVerticesTree(QTNode*node, int level);
		static void aux_printEdgesTree(QTNode*node, int level);
	public:
		TrMesh2D_TrBased_PRQTreeNewEntriesManager(TrMesh2D_TrBased* owner=NULL);
		void setOwner(TrMesh2D_TrBased* owner);
		int addingVertex(Point2D p, int new_id); /// returns the id of the vertex previously added or adds the new vertex and returns new_id (that should be the id of the point if it would be added)
		TT addingEdge(Point2D_int p, int tr_id, int tt_id); /// returns a TT with the id of the triangle that owns the edge and the relative id of TT if previously added, tr_id and tt_id if it's new or -1 if error. tr_id is the id of the owner triangle of e. tt is the id of the TT relation based on e for tr_id
		
		void printVerticesTree();
		void printEdgesTree();
	};
}
#endif
